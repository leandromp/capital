$(function () {
	$("#email, #pass").keyup(function(event){
	    if(event.keyCode == 13){
	        login();
	    }
	});
	$("#email_fp").bind('keypress', function(event){
	    if (event.keyCode == '13')
	    {
	        enviar_password();
	    }
	});
});

function login()
{
	$("#btnlogin").hide();
	$("#loading").show();
	$("#email").prop('disabled', true);
	$("#pass").prop('disabled', true);
	if (($("#email").val()!="") && ($("#pass").val()!=""))
	{
		$.post(URL_BASE+"usuarios/login",{email: $("#email").val(), pass: $("#pass").val()},function(data){
			if (data.estado==false)
			{
				$("#error").html(data.msj);
				$("#error").show(600).delay(2000).hide(600,function(){ $("#loading").hide();$("#btnlogin").show();$("#email").prop('disabled', false);$("#pass").prop('disabled', false);});
				//$("#error").faceIn(1000,function(){ $("#loading").hide(); $("#btnlogin").show();});
			}
			else
				location.reload();
		},'json');
	}
	else
	{
		$("#error").html('Tenes que ingresar tu correo y contraseña.');
		$("#error").show(600).delay(2000).hide(600,function(){ $("#loading").hide();$("#btnlogin").show();$("#email").prop('disabled', false);$("#pass").prop('disabled', false);});
	}
}

function enviar_password()
{
  $("#btn-enviar-fp").button('loading');
  $("#btn-cancel-fp").hide();
  $.post(URL_BASE+'usuarios/recordar_pass',{email: $("#email_fp").val()},function(data){
    switch(data.res)
    {
      case "ok":
        $("#message").addClass("text-green").removeClass("text-red");
        $("#message").html("<b>Envio completado!</b> "+data.msj);
        $("#message").show(600).delay(2000).hide(600);
      break;
      default:
        $("#message").addClass("text-red").removeClass("text-green");
        $("#message").html("<b>Oh error!</b> "+data.msj);
        $("#message").show(600).delay(2000).hide(600);
      break;
    }
    $("#btn-enviar-fp").button('reset');
    $("#btn-cancel-fp").show();
  },'json');
}