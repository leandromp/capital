$(function () {
	$("#tabla-busqueda").dataTable({
		"columnDefs": [
    		{
    			"width": "40%", "targets": 1,
    		}
  		],
		"language":
		{
			"search": "Buscar",
			 "info":  "Mostrando _START_ de _END_ de _TOTAL_ registros",
			 "paginate": {
		        "first":      "Primera",
		        "last":       "Ultima",
		        "next":       "Siguiente",
		        "previous":   "Anterior"
		    },
		    "lengthMenu":     "Mostrando _MENU_ registros",
		}
	});
});

function eliminar_accion(id,controlador)
{
	var accion;
	switch(controlador)
	{
		case 'fotos':
			accion = '/eliminar_foto';
			controlador = 'noticias';
		break;
		case 'perifericos_historico':
			accion = '/eliminar_historico';
			controlador = 'perifericos';
		break;
		case 'historico_equipos':
			accion = '/eliminar_historico';
			controlador = 'equipos';
		break;
		case 'nota_videos':
			accion = '/eliminar_video';
			controlador = 'noticias';
		break;
		case 'menu':
			accion = '/eliminar_menu';
			controlador = 'menus';
		break;
		case 'galeria_fotos':
			accion = '/eliminar_foto';
			controlador = 'galerias';
		break;
		default:
			accion = '/eliminar';
		break;
	}
	$.post(URL_BASE+controlador+accion,{id:id},function(data){
		switch (data)
		{
			case '1':
				$("#modal_titulo").html("Mensaje");
				$("#modal_mensaje").html("Se ha eliminado con exito el registro");
				$("#modal_botones").html('<button data-dismiss="modal" class="btn btn-default" type="button" id="btn-cancel-fp" data-loading-text="Cancelar">Cerrar</button>');
				$("#modalMensaje").modal();
				$("#fila_"+id).hide();
			break;
			case'	1':
				$("#modal_titulo").html("Mensaje");
				$("#modal_mensaje").html("Se ha eliminado con exito el registro");
				$("#modal_botones").html('<button data-dismiss="modal" class="btn btn-default" type="button" id="btn-cancel-fp" data-loading-text="Cancelar">Cerrar</button>');
				$("#modalMensaje").modal();
				$("#fila_"+id).hide();
			break;
			case '2':
				alert("hubo un error al intentar ejecutar la accion, avise al administrador del sistema");
			break;
		}
	});
}

function eliminar (id,controlador)
{
	$("#modal_titulo").html('');
	$("#modal_titulo").html("Mensaje");
	$("#modal_botones").html('');
	$("#modal_mensaje").html("Desea Eliminar el Registro "+id);
	$("#modal_botones").html('<button data-dismiss="modal" class="btn btn-default" type="button" id="btn-cancel-fp" data-loading-text="Cancelar">Cancelar</button>');
	$("#modal_botones").append('<button class="btn btn-primary" type="button" id="btn-enviar-fp" onclick="eliminar_accion('+id+',\''+controlador+'\')">Aceptar</button>');
	$("#modalMensaje").modal();
}

function mostrar_mensaje(titulo,texto,action)
{
	$("#modal_titulo").html(titulo);
	//$("#modal_botones").html('');
	$("#modal_mensaje").html(texto);
	if (action!="")
		$("#btn-cancel-fpa").click(action);
		//ir_a(action);
	$("#modalMensaje").modal();
}

function cambiar_perfil()
{
	var perfil=$("#perfil").val();
	$.post(URL_BASE+'modulos/cambiar_perfil',{perfil:perfil},function(data){
		$("#cuerpo").html(data);
	});
}

function agregar_permisos()
{
	var perfil_id = $("#perfil").val();
	if(perfil_id>0)
	{
		$("#myModal").modal();
		$.post(URL_BASE+'modulos/listar_modulos', {perfil_id: perfil_id}, function(data) {
			switch(data)
			{
				case '1':
					$("#modulos").html('<p> No existen Modulos habilitados para este usuario');
					$("#btn_agregar_modulo").hide();
				break;
				default:
					$("#modulos").html(data);
					$("myModal").modal('hide');
				break
			}
		});
	}
	else
		alert('debe seleccionar un perfil para poder acceder');
}

function agregar_modulo_perfil()
{
	var perfil_id = $("#perfil").val();
	var modulo_id = $("#modulo").val();
	$.post(URL_BASE+'modulos/agregar_modulo_perfil', {perfil_id:perfil_id,modulo_id:modulo_id}, function(data) {
		$("#cuerpo").html(data);
	});
}

function guardarGeneral(p_form_id,p_accion)
{
	if(validar(p_form_id))
	{
		$("#loading").show();
		$("#btnguardar").hide();
		var datos = $("#"+p_form_id).serializeArray();
		if(p_form_id=='notas-form' || p_form_id=='faq-form' || p_form_id=='paginas-form')
		{
			var text = CKEDITOR.instances.textonota.getData();
			datos.push({name:'texto',value:text});
		}


		$.post($("#"+p_form_id).attr('action'), datos,
		function(data)
		{
			if (!data.estado)
			{
				mostrar_mensaje('Error',data.msj,'');
				$("#btnguardar").show();
				$("#loading").hide();
			}
			else
			{
				console.log(p_accion);
				mostrar_mensaje('OK',data.msj, function(){ ir_a(p_accion); });
			}

		},'json');
	}
	else
		mostrar_mensaje('Error','Tenes que ingresar toda la información solicitada.','');
}

function validar(p_form_id)
{
	var res = true;
	$("#"+p_form_id).find(':input').each(function() {
		var elemento= this;
		if($(this).hasClass('require'))
		{
			if ((elemento.value=="") || (elemento.value==0))
			{
				nn = elemento.name;
				//$("#"+nn).addClass('has-error').delay(2000).removeClass('has-error');
				$("#"+nn).addClass('has-error');
				res = false;
			}
		}
	});

	return res;
}

function ir_a(p_accion)
{
	window.location = URL_BASE+p_accion;
}

function subir_imagen(p_metodo)
{
	var funcion = p_metodo.split('/');
  //$(".intext").removeClass("incorrecto");
  	$("#subir").hide();
    $("#subiendo").show();
    $('#foto-nota').form('submit',{
      url: URL_BASE+p_metodo,
      success: function(result){
        switch(result)
        {
        	case '1':
        		mostrar_mensaje('Exito','Se agregado la imagen con exito.','noticias/fotos') ;
        		ir_a(funcion[0]+'/fotos/'+funcion[2]);
        	break ;
        	case '2':
        		mostrar_mensaje('Error','No se pudo subir la imagen','noticias/fotos') ;
        	break;

        }
      }
    });
}

function subir_adjunto(id)
{
  //$(".intext").removeClass("incorrecto");
  	$("#subir").hide();
    $("#subiendo").show();
    $('#foto-nota').form('submit',{
      url: URL_BASE+'paginas/subir_adjunto',
      success: function(result){
        switch(result)
        {
        	case '1':
        		mostrar_mensaje('Exito','Se agregado el archivo con exito.',function(){ ir_a("paginas/adjuntos/"+id);}) ;
        	break ;
        	case '2':
        		mostrar_mensaje('Error','Error de DB comuniquelo al administrador',function(){ir_a('paginas/adjuntos/'+id);} ) ;
        	break;
        	case '3':
        		mostrar_mensaje('Error','No tiene los permisos para acceder al moodulo.',function(){ir_a('paginas/adjuntos/'+id);} ) ;
        	break;
        	case '4':
        		mostrar_mensaje('Error','El archivo seleccionado no es valido.',function(){ir_a('paginas/adjuntos/'+id);} ) ;
        	break;
        	case '5':
        		mostrar_mensaje('Error','No se ha podido subir el archivo',function(){ir_a('paginas/adjuntos/'+id);} ) ;
        	break;
        	case '6':
        		mostrar_mensaje('Error','El tama&ntilde;o del archivo supera el maximo permitido.',function(){ir_a('paginas/adjuntos/'+id);} ) ;
        	break;
        	default:
        		mostrar_mensaje('Error','Error por favor comuniquelo al Administrado del sistema.',function(){ir_a('paginas/adjuntos/'+id);} ) ;
        	break;
        }
        //ir_a('paginas/adjuntos/'+id);
      }
    });
}

function subir_adjunto_faq(id)
{
  //$(".intext").removeClass("incorrecto");
  	$("#subir").hide();
    $("#subiendo").show();
    $('#adjunto-faq').form('submit',{
      url: URL_BASE+'faqs/subir_adjunto',
      success: function(result){
        switch(result)
        {
        	case '1':
        		mostrar_mensaje('Exito','Se agregado el archivo con exito.','faqs/adjuntos/'+id) ;
        	break ;
        	case '2':
        		mostrar_mensaje('Error','Error de DB comuniquelo al administrador','faqs/adjuntos/'+id) ;
        	break;
        	case '3':
        		mostrar_mensaje('Error','No tiene los permisos para acceder al moodulo.','faqs/adjuntos/'+id) ;
        	break;
        	case '4':
        		mostrar_mensaje('Error','El archivo seleccionado no es valido.','faqs/adjuntos/'+id) ;
        	break;
        	case '5':
        		mostrar_mensaje('Error','No se ha podido subir el archivo','faqs/adjuntos/'+id) ;
        	break;
        	case '6':
        		mostrar_mensaje('Error','El tama&ntilde;o del archivo supera el maximo permitido.','faqs/adjuntos/'+id) ;
        	break;
        	default:
        		mostrar_mensaje('Error','Error por favor comuniquelo al Administrado del sistema.','faqs/adjuntos/'+id) ;
        	break;
        }
        ir_a('faqs/adjuntos/'+id);
      }
    });
}




/****************** edita el campo de una tabla ****************/

function edit(p_id, p_campo,p_tabla)
{
	var id_fila = '#'+p_campo+'_'+p_id;
	var texto = $(id_fila).html();
	$(id_fila).removeAttr('ondblclick');
	$(id_fila).html('');
	$(id_fila).append('<input id="temp" type="text" value="'+texto+'"">');
	$(id_fila).focusout(function(event) {
		valor = ($("#temp").val());
		$.post(URL_BASE+'genericos/update', {id:p_id, campo:p_campo, tabla:p_tabla, valor:valor}, function(data) {
			switch(data)
			{
				case '1':
					$(id_fila).html('');
					$(id_fila).html(valor);
					$(id_fila).attr('ondblclick','edit('+p_id+',"'+p_campo+'","'+p_tabla+'")');
				break;
			}


		});

	});

}
/*************************************************************/

function guardarMenu()
{
	    var datos = $('.dd').nestable('serialize');
	    $("#btn-cambios").hide();
	    $("#loading").show();
	    $.post(URL_BASE+'menus/guardar_configuracion',{datos:datos},function(data){
	    	switch (data)
	    	{
	    		case '1':
	    			mostrar_mensaje('Exito','Cambios Guardados Correctamente','menus/editar');
	    			$("#btn-cambios").show();
	    			$("#loading").hide();
	    		break;
	    	}
	    });
}

function editar_elemento_menu(p_elemento_id)
{
	$.post(URL_BASE+'menus/datos_elementos', {elemento_id:p_elemento_id}, function(data) {
		$('#menu_id_e').val(data.menu_id);
		$('#id').val(data.id);
		$('#titulo_e').val(data.titulo);
		$('#enlace_e').val(data.enlace_externo);
		$('#orden_e').val(data.orden);
		$('#pagina_id_e').val(data.objeto_id_enlace);
		$('#modalElementosEditar').modal();
	},'json');
}

function combo(p_id,p_campo,p_tabla)
{
	var respuesta = confirm('¿Desea cambiar la propiedad '+p_campo+' esta nota?');
	var id_combo = '#'+p_campo+'_'+p_id;
	var valor_actual = $(id_combo).prop('checked');
	console.log(valor_actual);
	if(respuesta == true)
		{
			var id='#'+p_campo;
			$.post(URL_BASE+'genericos/combo', {id:p_id, campo:p_campo, tabla:p_tabla}, function(data) {
					switch(data)
					{
						case '1':
							$(id_combo).val(data);
						break;
					}
				});
		}
	else
	{
		if(valor_actual==true)
			$(id_combo).prop('checked', false);
		else
			$(id_combo).prop('checked', true);
	}
}

/* Script para el armado del menu ordenable */
$(document).ready(function()
{
	$.fn.datepicker.dates['ES'] = {
    days: ["Domingo", "Lunes", "Martes", "Miercoles", "Jueves", "Viernes", "Sabado"],
    daysShort: ["Dom", "Lun", "Mar", "Mie", "Jue", "Vie", "Sab"],
    daysMin: ["Do", "Lu", "Ma", "Mi", "Ju", "Vi", "Sa"],
    months: ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"],
    monthsShort: ["Ene", "Feb", "Mar", "Abr", "May", "Jun", "Jul", "Ago", "Sep", "Oct", "Nov", "Dic"],
    today: "Today",
    clear: "Clear",
    format: "mm/dd/yyyy",
    titleFormat: "MM yyyy", /* Leverages same syntax as 'format' */
    weekStart: 0
	};



	$(".select2").select2({
		ajax: {
		    url: function (params) {
     			 return  URL_BASE+"ventas/listadoProductosVenta/"+params.term;
    			},
		    dataType: 'json',
		    delay: 250,
		    data: function (params) {
		      return {
		        //q: params.term, // search term
		       // page: params.page
		      };
		    },
		    processResults: function (data, params) {
		      // parse the results into the format expected by Select2
		      // since we are using custom formatting functions we do not need to
		      // alter the remote JSON data, except to indicate that infinite
		      // scrolling can be used
		      params.page = params.page || 1;

		      return {
		        results: data.items,
		        pagination: {
		          more: (params.page * 30) < data.total_count
		        }
		      };
		    },
		    cache: true
		  },
		  escapeMarkup: function (markup) { return markup; }, // let our custom formatter work
		  minimumInputLength: 3,
	});

	$(".select2").change(function() {
		var codigo_producto=$('#descripcion option:selected').val();
		$.post(URL_BASE+'ventas/getProductoVenta', {id:codigo_producto}, function(res) {
			console.log(res);
			$("#codigo").val(res[0].codigo);
			$("#precio").val(res[0].precio_venta);
			$("#stock").val(res[0].cantidad);
		},'json');
	});

	$(".selectnombre").select2({
		ajax: {
		    url: function (params) {
     			 return  URL_BASE+"ventas/listadoCLienteVenta/"+params.term;
    			},
		    dataType: 'json',
		    delay: 250,
		    data: function (params) {
		      return {
		        //q: params.term, // search term
		       // page: params.page
		      };
		    },
		    processResults: function (data, params) {
		      // parse the results into the format expected by Select2
		      // since we are using custom formatting functions we do not need to
		      // alter the remote JSON data, except to indicate that infinite
		      // scrolling can be used
		      params.page = params.page || 1;

		      return {
		        results: data.clientes,
		        pagination: {
		          more: (params.page * 30) < data.total_count
		        }
		      };
		    },
		    cache: true
		  },
		  escapeMarkup: function (markup) { return markup; }, // let our custom formatter work
		  minimumInputLength: 3,
	});

	$(".selectnombre").change(function() {
		var id_cliente=$('#rsocial option:selected').val();
		$.post(URL_BASE+'ventas/getClienteByIdVenta', {id:id_cliente}, function(res) {
			$("#cuit").val(res.clientes.cuit);
		},'json');
	});

	/* DEFINE EL EVENTO QUE AL PRESIONAR EL ENTER EN EL CLASS EJECUTE EL METODO UPDATE */



});

/**************************************** FUNCIONES PARA  LA VENTA *************************************************/
var total=0;
var id_fila=0;
function agregarProducto()
{

	var codigo = $('#codigo').val();
	var descripcion = $('#descripcion option:selected').text();
	var precio = $('#precio').val();
	var cantidad = parseInt($('#cantidad').val());
	var stock = parseInt($('#stock').val());
	if(cantidad<=stock)
	{
		if(codigo=='' || precio<=0 || descripcion=='' || descripcion==null)
		{
			$(".mensajes").html('debe buscar un producto');
			$(".mensajes").show();

			setTimeout(function(){
				$(".mensajes").hide();
			}, 4000);
		}
		else
		{
			if(cantidad>0)
			{
				parseInt(cantidad);
				parseFloat(precio);
				subtotal = precio * cantidad;
				total = total+subtotal;
				stock = stock - cantidad;
				id_fila++;
				var cadena_producto="<tr id='fila_"+id_fila+"'> <td id='codigo_"+id_fila+"'>"+codigo+"</td><td>"+descripcion+"</td><<td id='cantidad_"+id_fila+"'>"+cantidad+"</td><td id='precio_"+id_fila+"'>"+precio+"</td><td id='subtotal_"+id_fila+"'>"+subtotal+"</td><td onclick='eliminar_producto_venta("+id_fila+")' class='btn btn-sm btn-danger'>eliminar</td></tr>"
				$("#detalle-venta").append(cadena_producto);
				$(".total").html('Total: $'+total);
			}
			else
			{
				$(".mensajes").html('debe agregar una cantidad para el producto seleccionado');
				$(".mensajes").show();

				setTimeout(function(){
					$(".mensajes").hide();
				}, 4000);
			}

		}
	}
	else
	{
		$(".mensajes").html('No tiene suficientes productos en stock para realizar la operacion.');
		$(".mensajes").show();
		setTimeout(function(){
			$(".mensajes").hide();
		}, 4000);
	}


}

function eliminar_producto_venta(p_id_fila)
{
	var subtotal = $("#subtotal_"+p_id_fila).text();
	var total = $(".total").html();
	var total = total.substr(8,8);
	total = total - subtotal;
	$(".total").html('Total: $'+total);
	$("#fila_"+p_id_fila).remove();
}

function guardar_venta()
{
	/* capturo los valores de datos del cliente */
	var cuit = $('#cuit').val();
	var rsocial = $('#rsocial').text();
	var fecha = $("#fecha").val();
	var total = $("#total").html();
	var total = total.substr(8,8);
	$("#btnguardar").hide();
	$("#loading").show();
	//console.log(cuit+rsocial);
	if(cuit=='' || cuit==null || fecha=='')
	{
		$(".mensajes").html('No se puede guardar la venta, no hay un cuit seleccionado, o no ha ingresado una fecha.');
			$(".mensajes").show();

			setTimeout(function(){
				$(".mensajes").hide();
			}, 4000);
		$("#btnguardar").show();
		$("#loading").hide();
	}
	else
	{
		var productos=[];
		//capturo los datos del detalle de venta.
		$("tr").each(function(index, el) {
			var elemento = $(this).attr('id');
			var id = elemento.substr(5,5);
			if(id>0)
			{
				var obj={};
				obj['codigo']=$("#codigo_"+id).text();
				obj['cantidad']=$("#cantidad_"+id).text();
				obj['precio']=$("#precio_"+id).text();
				productos.push(obj);
			}

		});

		$.post(URL_BASE+'ventas/guardar', {fecha:fecha,total:total,cuit:cuit,productos: JSON.stringify(productos)}, function(data) {

			switch(data.res)
			{
				case 'ok':
					mostrar_mensaje('MENSAJE','Venta guardada Correctamente','');
					setTimeout(function(){ ir_a('ventas')}, 4000);
				break;
				case 'productos':
					$(".mensajes").html('Debe tener al menos 1 producto para poder guardar la venta.');
					$(".mensajes").show();
					setTimeout(function(){ $(".mensajes").hide();}, 4000);
					$("#btnguardar").show()
					$("#loading").hide();
				break;
				case 'error_db':
					$(".mensajes").html('Error en la db comuniquelo al Administrador.');
					$(".mensajes").show();
					setTimeout(function(){ $(".mensajes").hide();}, 4000);
					$("#btnguardar").show()
					$("#loading").hide();
				break
			}

		},'json');
	}
}
/********************************* FUNCIONES PARA EL PAGO DE UNA VENTA ***********************************************/
function agregar_pago()
{
	var cantidad = $("#cantidad").val();
	var cliente_cuit = $("#cliente_cuit").val();
	$.post(URL_BASE+'pagos/guardar_pago', {cliente_cuit:cliente_cuit,cantidad:cantidad}, function(data) {
		ir_a('pagos/ver_cuenta/'+cliente_cuit);
	});
}


function agregar_historico()
{
	var descripcion = $("#descripcion").val();
	var equipo_id=$("#equipo_id").val();
	var fecha=$("#fecha").val();

	$.post(URL_BASE+'equipos/guardar_historico', {descripcion:descripcion, fecha:fecha, equipo_id:equipo_id}, function(data)
	{
		if(data.estado==1)
			mostrar_mensaje2('Mensaje',data.msj,'equipos/historico/'+equipo_id);
		else
			mostrar_mensaje2('Mensaje',data.msj,'equipos/historico/'+equipo_id);
	},'json');
}

function agregar_historico_periferico()
{
	var descripcion = $("#descripcion").val();
	var equipo_id=$("#equipo_id").val();
	var fecha=$("#fecha").val();

	$.post(URL_BASE+'perifericos/guardar_historico', {descripcion:descripcion, fecha:fecha, equipo_id:equipo_id}, function(data)
	{
		if(data.estado==1)
			mostrar_mensaje2('Mensaje',data.msj,'perifericos/historico/'+equipo_id);
		else
			mostrar_mensaje2('Mensaje',data.msj,'perifericos/historico/'+equipo_id);
	},'json');
}
/********************************************************************************************************************/

function mostrar_mensaje2(titulo,texto,action)
{
	$("#modal_titulo").html(titulo);
	//$("#modal_botones").html('');
	$("#modal_mensaje").html(texto);
	$("#modalMensaje").modal();
	if (action!="")
		$("#btn-cancel-fpa").attr('onclick','ir_a("'+action+'")');
		//ir_a(action);


}

function eliminar_configuracion(item,codigo)
{
	$.post(URL_BASE+'configuraciones/eliminar',{item:item,codigo:codigo},function(data){
		switch (data)
		{
			case '1':
				$("#modal_titulo").html("Mensaje");
				$("#modal_mensaje").html("Se ah eliminado con exito el registro");
				$("#modal_botones").html('<button data-dismiss="modal" class="btn btn-default" type="button" id="btn-cancel-fp" data-loading-text="Cancelar">Cerrar</button>');
				$("#modalMensaje").modal();
				$("#fila_"+item).hide();
			break;
			case'	1':
				$("#modal_titulo").html("Mensaje");
				$("#modal_mensaje").html("Se ah eliminado con exito el registro");
				$("#modal_botones").html('<button data-dismiss="modal" class="btn btn-default" type="button" id="btn-cancel-fp" data-loading-text="Cancelar">Cerrar</button>');
				$("#modalMensaje").modal();
				$("#fila_"+item).hide();
			break;
			case '2':
				alert("hubo un error al intentar ejecutar la accion, avise al administrador del sistema");
			break;
		}
	});
}


function agregar_puesto()
{
	var puesto_id=$("select#puesto").val();
	var fecha = $("#fecha").val();
	var empleado_id = $("#empleado_id").val();
	var actual = $("#actual").is(':checked');
	
	$.post(URL_BASE+'empleados/agregar_puesto',{puesto_id:puesto_id,fecha:fecha,empleado_id:empleado_id,actual:actual},function(data){
		console.log(data);
			if(data=='ok')
				window.location=URL_BASE+'empleados/puesto/'+empleado_id;
			else{
				$("#error").html('el empleado ya tiene ese puesto asignado');
				$("#error").css('visibility', 'visible');
			}
	});
}

function agregar_nivel()
{
	var nombre = $("#nivel_nombre").val();
	var id_departamento = $("#id").val();
	if (id_departamento=="")
		var url = 'alta'
	else
		var url = 'editar/'+id_departamento;
	
	$.post(URL_BASE+'niveles/guardar',{descripcion:nombre},function(data){
			window.location=URL_BASE+'departamentos/'+url;
	});
}

function mostrarEmpleadosByDepartamento()
{
	var id_departamento=$("#id_departamento").val();
	if(id_departamento>0)
		window.open(URL_BASE+'pdfs/listar_empleados_by_departamento/'+id_departamento, '_blank');
	else
		alert('debe seleccionar un departamento');
}