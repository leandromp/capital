<?
class Archivos
{
	public $file;
	public $path;
	public $tipos;
	public $imageresize;
	public $nombre_img;
	public $min_ancho;
	public $min_alto;
	public $max_peso;
	
	function __construct()
	{
		
	}
	
	public function subir()
	{
		if($this->file['error'] == 0)
		{
			if($this->file['size'] <= $this->max_peso)
			{
				$extension = strtolower(end(explode(".",$this->file['name'])));
				$extension;
				$tipos_permitidos = explode(",",$this->tipos);
				if (in_array($extension,$tipos_permitidos))
				{
					$nombre = $this->nombre_img.".".$extension;
					$ruta_absoluta = PATH_SITIO . $this->path;
					if (!is_dir($ruta_absoluta))
						mkdir($ruta_absoluta,0777,true);
							
					$info_imagen=getimagesize($this->file['tmp_name']);
					$ancho = $info_imagen[0];
					$alto = $info_imagen[1];
					if ($ancho >= $this->min_ancho and $alto >= $this->min_alto)
					{
						move_uploaded_file($this->file['tmp_name'],$ruta_absoluta.$nombre);
							
						/*Diferentes tamanios*/
						$dest_size0 = $ruta_absoluta.$nombre;
						$dest_size1= $ruta_absoluta."tmb1_".$nombre;
						$dest_th = $ruta_absoluta."th_".$nombre;
						$dest_list=$ruta_absoluta."list_".$nombre;
						/**********************/
						
						//TAM_0_W
						if ($ancho > TAM_0_W)
						{		
							$this->imageresize->setImage($ruta_absoluta.$nombre);
							//$this->imageresize->resizeWidth(TAM_0_W); // 787
							$this->imageresize->resizeWidthHeight('787' , '524');
							$this->imageresize->save($dest_size0);
						}
						else
						{
							$this->imageresize->setImage($ruta_absoluta.$nombre);
							$this->imageresize->resizeWidth($ancho); 
							$this->imageresize->save($dest_size0);
						}
						
						//TAM_1_W
						$this->imageresize->setImage($ruta_absoluta.$nombre);
						$this->imageresize->resizeWidth(TAM_1_W);
						$this->imageresize->save($dest_size1);
						
						//TAM_TH_W
						$this->imageresize->setImage($ruta_absoluta.$nombre);
						$this->imageresize->resizeWidth(TAM_TH_W); // 150
						$this->imageresize->save($dest_th);
						//TAM_LIST
						$this->imageresize->setImage($ruta_absoluta.$nombre);
						$this->imageresize->resizeWidth(TAM_LIST); // 460
						$this->imageresize->save($dest_list);
						

						$img = $nombre;
						$error = "";
					}
					else
					{
						$img = "";
						$error = "error_tamanioimg";
					}
				}
				else
				{
					$img = "";
					$error = "error_formatoimg";
				}
			}
			else
			{
				$img = "";
				$error = "error_pesoimg";
			}
		}
		else
		{
			$img = "";
			$error = "error_pesoimg";
		}
		
		$return['img'] = $img;
		$return['error'] = $error;
		
		return $return;
	}

	public function subir_avatar()
	{
		if($this->file['error'] == 0)
		{
			if($this->file['size'] <= $this->max_peso)
			{
				$extension = strtolower(end(explode(".",$this->file['name'])));
				$tipos_permitidos = explode(",",$this->tipos);
				if (in_array($extension,$tipos_permitidos))
				{
					$nombre = $this->nombre_img.".".$extension;
					$ruta_absoluta = PATH_BASE_FILE . $this->path;
					if (!is_dir($ruta_absoluta))
						mkdir($ruta_absoluta);
							
					$info_imagen=getimagesize($this->file['tmp_name']);
					$ancho = $info_imagen[0];
					$alto = $info_imagen[1];
					if ($ancho >= $this->min_ancho and $alto >= $this->min_alto)
					{
						
						move_uploaded_file($this->file['tmp_name'],$ruta_absoluta."gr_".$nombre);
							
						//TAM_AV_W
						/*$this->imageresize->setImage($ruta_absoluta."gr_".$nombre);
						$this->imageresize->resizeWidthHeight(AV_W, AV_W); // 150
						$this->imageresize->save($ruta_absoluta.$nombre);*/

						if ($ancho > 500)
						{
							//500
							$this->imageresize->setImage($ruta_absoluta."gr_".$nombre);
							$this->imageresize->resizeWidth(500); // 150
							$this->imageresize->save($ruta_absoluta."gr_".$nombre);
						}
						
						$img = $nombre;
						$error = "";
					}
					else
					{
						$img = "";
						$error = "error_tamanioimg";
					}
				}
				else
				{
					$img = "";
					$error = "error_formatoimg";
				}
			}
			else
			{
				$img = "";
				$error = "error_pesoimg";
			}
		}
		else
		{
			$img = "";
			$error = "error_pesoimg";
		}
		
		$return['img'] = $img;
		$return['error'] = $error;
		
		return $return;
	}

	public function subir_archivo()
	{
		if($this->file['error'] == 0)
		{
			if($this->file['size'] <= $this->max_peso)
			{
				$extension = strtolower(end(explode(".",$this->file['name'])));
				$tipos_permitidos = explode(",",$this->tipos);
				if (in_array($extension,$tipos_permitidos))
				{
					$nombre = $this->nombre_img.".".$extension;
					$ruta_absoluta = PATH_BASE_FILE . $this->path;
					if (!is_dir($ruta_absoluta))
						mkdir($ruta_absoluta);

					move_uploaded_file($this->file['tmp_name'],$ruta_absoluta.$nombre);
					$img = $nombre;
					$error = "";
				}
				else
				{
					$img = "";
					$error = "error_formatoarch";
				}
			}	
			else
			{
				$img = "";
				$error = "error_pesoarch";		
			}
		}
		else
		{
			$img = "";
			$error = "error_pesoarch";
		}

		$return['img'] = $img;
		$return['error'] = $error;
		
		return $return;
	}

	public function crop($x,$y,$h,$w)
	{
		$origen = PATH_BASE_FILE.$this->path."gr_".$this->nombre_img;
		if (file_exists($origen))
		{
			$info_imagen = getimagesize($origen);
			$extension = strtolower(end(explode(".",$this->nombre_img)));
			
			if ($extension=="jpg") $img = imagecreatefromjpeg($origen);
			if ($extension=="jpeg") $img = imagecreatefromjpeg($origen);
			
			$res = imagecreatetruecolor($w, $h);
			imagecopy($res, $img, 0, 0, $x, $y, $w, $h);
			
			$crop_temp=PATH_BASE_FILE."fotos/avatar_lector/temp/crop_".$this->nombre_img;
			$nuevoDestino=PATH_BASE_FILE.$this->path_destino.$this->nombre_img;
			
			if ($extension=="jpg") $proc_img = imagejpeg(@$res,$crop_temp,80);
			if ($extension=="jpeg") $proc_img = imagejpeg(@$res,$crop_temp,80);
			
			if ($proc_img)
			{				
				$this->imageresize->setImage($crop_temp);
				$this->imageresize->resizeWidth(200);
				$this->imageresize->save($nuevoDestino);

				unlink($crop_temp);
				imagedestroy($res);
				return true;
			}
			else
				return false;
		}
		else
			return false;
	}
}
?>