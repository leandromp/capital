<?php
 if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Varios_library
{
	private $ci;
	private $client_type;

	public function extraer_string($str, $left, $right)
	{
		$str = substr(stristr($str, $left), strlen($left));
		$leftLen = strlen(stristr($str, $right));
		$leftLen = $leftLen ? -($leftLen) : strlen($str);
		$str = substr($str, 0, $leftLen);
		return $str;
	}

	public function limpiarurl($frase)
	{
		$url = trim($frase);
		$url = str_replace("á","a",$url);
		$url = str_replace("é","e",$url);
		$url = str_replace("í","i",$url);
		$url = str_replace("ó","o",$url);
		$url = str_replace("ú","u",$url);
		$url = str_replace("ü","u",$url);
		$url = str_replace("Ñ","N",$url);
		$url = str_replace("ñ","n",$url);
		$url = str_replace(" un "," ",$url);
		$url = str_replace("Un ","",$url);
		$url = str_replace(" a "," ",$url);
		$url = str_replace(" su "," ",$url);
		$url = str_replace("Su ","",$url);
		$url = str_replace(" es "," ",$url);
		$url = str_replace("Es ","",$url);
		$url = str_replace(" una "," ",$url);
		$url = str_replace(" una","",$url);
		$url = str_replace("Una ","",$url);
		$url = str_replace(" la "," ",$url);
		$url = str_replace("La ","",$url);
		$url = str_replace(" las "," ",$url);
		$url = str_replace("Las ","",$url);
		$url = str_replace(" los "," ",$url);
		$url = str_replace("Los ","",$url);
		$url = str_replace(" el "," ",$url);
		$url = str_replace("El ","",$url);
		$url = str_replace(" del "," ",$url);
		$url = str_replace(" con "," ",$url);
		$url = str_replace(" por "," ",$url);
		$url = str_replace(" en "," ",$url);
		$url = str_replace("En ","",$url);
		$url = str_replace(" que "," ",$url);
		$url = str_replace(" y "," ",$url);
		$url = str_replace(" de "," ",$url);
		$url = str_replace(",","",$url);
		$url = str_replace(";","",$url);
		$url = str_replace(":","",$url);
		$url = str_replace("%","por-ciento",$url);
		$url = str_replace(".","",$url);
		$url = str_replace("@","",$url);
		$url = str_replace("?","",$url);
		$url = str_replace("¿","",$url);
		$url = str_replace("!","",$url);
		$url = str_replace("¡","",$url);
		$url = str_replace("“","",$url);
		$url = str_replace("”","",$url);
		$url = str_replace("¡","",$url);
		$url = str_replace("°","",$url);
		$url = str_replace("#","",$url);
		$url = str_replace("'","",$url);
		$url = str_replace('"','',$url);
		$url = str_replace("(","",$url);
		$url = str_replace(")","",$url);
		$url = str_replace("-","",$url);
		$url = str_replace("/","-",$url);
		$url = str_replace("´","",$url);
		$url = str_replace("  "," ",$url);
		$url = str_replace(" ","-",$url);
		$url = str_replace("›","",$url);
		$url = str_replace("|","",$url);
		$url = str_replace('​','',$url);//limpia el simbolo invisible
		$url = preg_replace("/[^A-Za-z0-9-]/",'', $url);

		$url = strtolower($url);
		$url = str_replace("--", "-", $url);

		return $url;
	}

	//YYYY-mm-dd a dd/mm/YYYY
	public function transformar_fecha($fecha,$separador="/")
	{
		if ($fecha != "")
		{
			$dia = substr($fecha, 8, 2);
			$mes = substr($fecha, 5, 2);
			$anio = substr($fecha, 0, 4);
			return $dia.$separador.$mes.$separador.$anio;
			/*$fecha_info = explode("-",$fecha);
			if (count($fecha_info) == 3)
			{
				$dia = $fecha_info[2];
				$mes = $fecha_info[1];
				$anio = $fecha_info[0];
				return $dia.$separador.$mes.$separador.$anio;
			}
			else
				return "";*/
		}
		else
			return "";
	}

	/*dd/mm/YYYY a YYYY-mm-dd*/
	public function transformar_fecha_inversa($fecha,$separador="/")
	{
		if ($this->validaFecha($fecha,$separador))
		{
			$dia = substr($fecha, 0, 2);
			$mes = substr($fecha, 3, 2);
			$anio = substr($fecha, 6, 4);

			return $anio."-".$mes."-".$dia;
		}
		else
			return "";
	}

	//dd/mm/YYYY
	public function validaFecha($date,$separador="/")
	{
		$day = substr($date, 0, 2);
		$mes = substr($date, 3, 2);
		$anio = substr($date, 6, 4);
		if (strlen($date) == 10)
		{
			if ($day <= 31)
			{
				if ($mes == "01" or $mes == "03" or $mes == "05" or $mes == "07" or $mes == "08" or $mes == "10" or $mes == "12")
				{
					return true;
				}
				else
				{
					if (($mes == "04" or $mes == "06" or $mes == "09" or $mes == "11") and $day <= "30")
					{
						return true;
					}
					else
					{
						if ($mes == "02")
						{
							if (fmod($anio,4) == 0 and $day <= 29)
								return true;
							else
								if ($day <= 28)
									return true;
								else
									return false;
						}
						else
							return false;
					}
				}
			}
			else
				return false;
		}
		else
			return false;
	}

	//Formato de fecha aaaa/mm/dd
	public function validaFechaInversa($date,$separador="/")
	{
		$fecha = explode($separador,$date);
		if (strlen($date) == 10 and count($fecha) == 3)
		{
			$day = $fecha[2];
			$mes = $fecha[1];
			$anio = $fecha[0];
			if ($day <= 31)
			{
				if ($mes == "01" or $mes == "03" or $mes == "05" or $mes == "07" or $mes == "08" or $mes == "10" or $mes == "12")
				{
					return true;
				}
				else
				{
					if (($mes == "04" or $mes == "06" or $mes == "09" or $mes == "11") and $day <= "30")
					{
						return true;
					}
					else
					{
						if ($mes == "02")
						{
							if (fmod($anio,4) == 0 and $day <= 29)
								return true;
							else
								if ($day <= 28)
									return true;
								else
									return false;
						}
						else
							return false;
					}
				}
			}
			else
				return false;
		}
		else
			return false;
	}

	public function cortar_texto($string, $limit, $break=".", $pad="…")
	{
		// return with no change if string is shorter than $limit
		if(strlen($string) <= $limit)
		return $string;
		// is $break present between $limit and the end of the string?
		if(false !== ($breakpoint = strpos($string, $break, $limit)))
		{
			if($breakpoint < (strlen($string)-1))
			{
				$string = substr($string, 0, $breakpoint) . $pad;
			}
		}
		return $string;
	}

	public function is_bot()
	{

		$user_agent = $_SERVER['HTTP_USER_AGENT'];
		//var_dump($user_agent);
	  //if no user agent is supplied then assume it's a bot
	  if($user_agent == "")
	    return 1;

	  //array of bot strings to check for
	  $bot_strings = Array(  "google",     "bot",
	            "yahoo",     "spider",
	            "archiver",   "curl",
	            "python",     "nambu",
	            "twitt",     "perl",
	            "sphere",     "PEAR",
	            "java",     "wordpress",
	            "radian",     "crawl",
	            "yandex",     "eventbox",
	            "monitor",   "mechanize",
	            "facebookexternal"
	          );
	  foreach($bot_strings as $bot)
	  {
	    if(strpos($user_agent,$bot) !== false)
	    { return 1; }
	  }

	  return 0;
	}

	public function validaEmail($email)
	{
	    $mail_correcto = 0;
	    //compruebo unas cosas primeras
	    if ((strlen($email) >= 6) && (substr_count($email,"@") == 1) && (substr($email,0,1) != "@") && (substr($email,strlen($email)-1,1) != "@")){
	       if ((!strstr($email,"'")) && (!strstr($email,'"')) && (!strstr($email,'\\')) && (!strstr($email,'$')) && (!strstr($email,' '))) {
	          //miro si tiene caracter .
	          if (substr_count($email,".")>= 1){
	             //obtengo la terminacion del dominio
	             $term_dom = substr(strrchr ($email, '.'),1);
	             //compruebo que la terminación del dominio sea correcta
	             if (strlen($term_dom)>1 && strlen($term_dom)<5 && (!strstr($term_dom,"@")) ){
	                //compruebo que lo de antes del dominio sea correcto
	                $antes_dom = substr($email,0,strlen($email) - strlen($term_dom) - 1);
	                $caracter_ult = substr($antes_dom,strlen($antes_dom)-1,1);
	                if ($caracter_ult != "@" && $caracter_ult != "."){
	                   $mail_correcto = 1;
	                }
	             }
	          }
	       }
	    }
	    if ($mail_correcto)
	       return 1;
	    else
	       return 0;
	}

	public function diasDiferencia($fecha_mayor, $fecha_menor)
	{
		if ($this->validaFecha($fecha_mayor) and $this->validaFecha($fecha_menor))
		{
			$dia_mayor = substr($fecha_mayor, 0, 2);
			$mes_mayor = substr($fecha_mayor, 3, 2);
			$anio_mayor = substr($fecha_mayor, 6, 4);

			$dia_menor = substr($fecha_menor, 0, 2);
			$mes_menor = substr($fecha_menor, 3, 2);
			$anio_menor = substr($fecha_menor, 6, 4);

			$segundos_mayor = mktime(0,0,0,$mes_mayor,$dia_mayor,$anio_mayor);
			$segundos_menor = mktime(0,0,0,$mes_menor,$dia_menor,$anio_menor);

			$diferencia_segundos = $segundos_mayor - $segundos_menor;
			if ($diferencia_segundos < 0)
			{
				$diferencia_segundos = $diferencia_segundos * (-1);
				return 0;
			}
			elseif($diferencia_segundos > 0)
			{
				$diferencia_dias = $diferencia_segundos / (60 * 60 * 24);
				return $diferencia_dias;
			}
			else
				return 0;
		}
		else
			return 0;
	}

	public function diasDiferenciaConcurso($fecha_mayor, $fecha_menor)
	{
		if ($this->validaFecha($fecha_mayor) and $this->validaFecha($fecha_menor))
		{
			$dia_mayor = substr($fecha_mayor, 0, 2);
			$mes_mayor = substr($fecha_mayor, 3, 2);
			$anio_mayor = substr($fecha_mayor, 6, 4);

			$dia_menor = substr($fecha_menor, 0, 2);
			$mes_menor = substr($fecha_menor, 3, 2);
			$anio_menor = substr($fecha_menor, 6, 4);

			$segundos_mayor = mktime(0,0,0,$mes_mayor,$dia_mayor,$anio_mayor);
			$segundos_menor = mktime(0,0,0,$mes_menor,$dia_menor,$anio_menor);

			$diferencia_segundos = $segundos_mayor - $segundos_menor;
			if ($diferencia_segundos < 0)
			{
				return -1;
			}
			elseif($diferencia_segundos > 0)
			{
				$diferencia_dias = $diferencia_segundos / (60 * 60 * 24);
				return $diferencia_dias;
			}
			else
				return 0;
		}
		else
			return 0;
	}



	public function restarDiasFecha($fecha,$dia,$separador="-")
	{
		//list($day,$mon,$year) = explode($separador,$fecha);
		$day = substr($fecha, 0,2);
		$mon = substr($fecha, 3,2);
		$year = substr($fecha, 6,4);
    	return date('Y-m-d',mktime(0,0,0,$mon,$day-$dia,$year));
	}

	public function calcularEdad($fecha)
	{
		if ($fecha)
		{
			$dia = substr($fecha,0,2);
			$mes = substr($fecha,3,2);
			$anio = substr($fecha,6,4);

			$dif = date('Y') - $anio;
			if ($mes > date('m'))
				$dif--;
			if ($mes == date('m') and $dia > date('d'))
				$dif--;

			return $dif;
		}
		else
			return 0;
	}

	public function validaHora($hora)
	{
		if ($hora!="")
		{
			$H = substr($hora, 0,2);
			$m = substr($hora, 3,2);
			$separador = substr($hora, 2,1);
			if ($separador==":")
			{
				if (is_numeric($H))
				{
					if ($H >= 0 and $H <= 23)
					{
						if (is_numeric($m))
						{
							if ($m >=0 and $m <=59)
								return true;
							else
								return false;
						}
						else
							return false;
					}
					else
						return false;
				}
				else
					return false;
			}
			else
				return flase;
		}
		else
			return false;
	}
}
?>
