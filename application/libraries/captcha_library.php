<?
 if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Captcha_library
{
	public function crear_captcha($key)
	{
		header('Content-Type: image/gif');
		// Se genera el área del gráfico
		$grafico = imagecreate(70, 22);
		// El primer color establecido será el color de fondo
		$fondo = imagecolorallocate($grafico, 0, 102, 153);
		// El siguiente color establecido será el color del texto
		$color = imagecolorallocate($grafico, 244, 244, 244);
		// Se establece el margen inicial para cada caracter escrito
		$margen = 5;
		// Se obtiene cada caracter de la cadena usando el loop for()
		for($x = 0; $x < strlen($key); $x++) {
			// Se extrae la cadena usando la función substr()
			$c = substr($key,$x,1);
			// La inclinación será 10 o -10 segun sea si el número de caracter es par o no
			if(($x % 2)==0) { $rend = 10; } else { $rend = -10; }
			// Se escribe el caracter en el gráfico
			imagettftext($grafico, 12, $rend, $margen, 16, $color, PATH_BASE_FILE.'css/RobotoSlab-Bold-webfont.ttf', $c);
			// Se incrementa el margen del siguiente caracter a escribir, en caso de existir
			$margen += 16;
		}
		// Se obtiene el gráfico para mostrar en el navegador
		imagegif($grafico);
		// Destruye la imagen creada liberando la memoria
		imagedestroy($grafico);	
	}
}
?>