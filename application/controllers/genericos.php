<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Genericos extends CI_Controller {

	function __construct()
	{
		parent:: __construct();
	}

	public function update()
	{
		$campo = $this->input->post("campo");
		$valor = $this->input->post("valor");
		$id = $this->input->post("id");
		$tabla = $this->input->post("tabla");
		$this->load->model('generico','generico',true);
		if ($this->generico->update($campo,$valor,$id,$tabla))
			echo 1;
		else
			echo 2;
	}

	public function combo()
	{
		$campo = $this->input->post("campo");
		$id = $this->input->post("id");
		$tabla = $this->input->post("tabla");
		$this->load->model('generico','generico',true);
		$valor= $this->generico->getValueId($id,$campo,$tabla);
		if($valor=='1')
			$valor_update=0;
		else
			$valor_update=1;
		if ($this->generico->update($campo,$valor_update,$id,$tabla))
			echo 1;
		else
			echo 2;
	}

}
