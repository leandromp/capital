<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	
class Usuarios extends CI_Controller 
{	
	function __construct()
	{
		parent::__construct();
		header ("Expires: Thu, 27 Mar 1980 23:59:00 GMT");
        header ("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
        header ("Cache-Control: no-cache, must-revalidate");
        header ("Pragma: no-cache");
        $this->pagina = $this->uri->segment(1);
	}
	
	public function _vistas($variables="")
	{
		$user=$this->session->userdata("usrpanel");
		if($user['usuario_id']>0)
		{
			$variables['usuario'] = $user;
			$this->load->library('menu');
			$variables['menu'] = $this->menu->dame_menu();
			$variables['nombre_pagina'] = $this->pagina;
			if ($variables['permiso'])
			{
				$this->load->view('usuarios/listado',$variables);
			}
			elseif($variables['vista']=='form-inc' and $user['usuario_id']==$variables['user']['id'])
			{
				$this->load->view('usuarios/listado',$variables);
			}
			else
			{
				$variables['error'] = 'No tiene permisos para acceder a esta modulo';
				$this->load->view('dashboard/inicio',$variables);
			}
		}
		else
			header("location: ".site_url())	;
	}

	public function index($variables="")
	{
		$user=$this->session->userdata("usrpanel");
		if($user['usuario_id']>0)
		{
			$this->load->model("usuario","usuario",true);
			$permiso = $this->usuario->getPermisos($user['perfil_id'],$this->pagina);
			$variables['permiso'] = $permiso['listado'];
			$variables['usuarios'] = $this->usuario->getListado();
			$this->_vistas($variables);
		}
		else
			header("location:".site_url());
	}

	public function alta()
	{
		$user=$this->session->userdata("usrpanel");
		if($user['usuario_id']>0)
		{
			$this->load->model("usuario","usuario",true);
			$permiso = $this->usuario->getPermisos($user['perfil_id'],$this->pagina);
			$variables['permiso'] = $permiso['alta'];
			$variables['perfiles'] =  $this->usuario->getPerfiles();
			$variables['vista']="form-inc";
			$this->_vistas($variables); 
		}
		else
			header("location:".site_url());
	}

	public function editar($id=0)
	{
		$user=$this->session->userdata("usrpanel");
		if($user['usuario_id']>0)
		{
			$this->load->model("usuario","usuario",true);
			$permiso = $this->usuario->getPermisos($user['perfil_id'],$this->pagina);
			$variables['permiso'] = $permiso['mod'];
			$variables['perfiles'] =  $this->usuario->getPerfiles();
			$variables['vista']="form-inc";
			$variables['user']=$this->usuario->getUsuarioId($id);
			$this->_vistas($variables);
		}
		else
			header("location:".site_url());
	}

	public function guardar()
	{
		$user=$this->session->userdata("usrpanel");
		$res['estado'] = 0;
		if($user['usuario_id']>0)
		{
			$this->load->model("usuario","usuario",true);
			$permisos=$this->usuario->getPermisos($user['perfil_id'],$this->pagina);
			$id=$this->input->post("id");
			$permiso = ($id)?$permisos['mod']:$permisos['alta'];
						
			if ($permiso or $user['usuario_id']==$id)
			{
				$datos["nombre"] = $this->input->post("nombre");
				$datos["apellido"] = $this->input->post("apellido");
				$datos["email"] = $this->input->post("email");
				$datos["password"] = $this->input->post("password");
				$datos["perfil_id"] = $this->input->post("perfil_id");
				if (!$permiso)
					unset($datos["perfil_id"]);

				if ($this->usuario->validaEmail($datos['email'],$id))
				{
					foreach ($datos as $key => $value) 
					{
						if(!$value)
						{
							if ($key!="password" or ($key=="password" and !$id))
							{
								$res['msj'] = 'Tenes que ingresar toda la información solicitada';
								echo json_encode($res);
								die();
							}
						}
					}
					$datos["habilitado"] = $this->input->post("habilitado");
					if($datos['habilitado'])
						$datos['habilitado']=1;
					else
						$datos['habilitado']=0;

					if (!$datos['password'])
						unset($datos['password']);
					else
						$datos['password'] = md5($datos['password']);

					if ($this->usuario->save($id,$datos))
					{
						$res['estado'] = 1;
						$res['msj'] = 'La operación fue realizada con éxito.';

						if ($id == $user['id'])
						{
							$user_ses = $this->session->userdata('usrpanel');
							$user_ses['nombre'] = $datos['nombre'];
							$user_ses['apellido'] = $datos['apellido'];
							$user_ses['email'] = $datos['email'];
							$this->session->set_userdata('usrpanel',$user_ses);
						}
					}
					else
						$res['msj'] = 'No se pudo finalizar la operación solicitada. Asegurate estar conectado a Internet.';
				}
				else
					$res['msj'] = 'El correo ingresado ya existe asociado a una cuenta de usuario.';
			}
			else
				$res['msj'] = "No tenes permiso para realizar la operación solicitada.";
		}
		else
			$res['msj'] = "Tu sesión ha finalizado. Tenes que iniciarla nuevamente.";

		echo json_encode($res);
	}

	public function login()
	{
		$correo=$this->input->post("email");
		$password=$this->input->post("pass");
		$ret['estado'] = false;
		if($correo and $password)
		{
			$this->load->model("usuario","usuario",true);
			$resultado=$this->usuario->comprobarLogeo($correo,$password);
			if ($resultado)
			{
				if ($resultado['habilitado'])
				{
					$session['id'] = $resultado['id'];
					$session['nombre']=$resultado['nombre'];
					$session['apellido']=$resultado['apellido'];
					$session['email']=$resultado['email'];
					$session['ultimo_acceso'] = $resultado['ultimo_acceso'];
					$session['usuario_id'] = $resultado['id'];
					$session['perfil_id'] = $resultado['perfil_id'];
					$session['perfil'] = $resultado['perfil'];
					$session['perfil_codigo'] = $resultado['codigo'];
					$this->session->set_userdata('usrpanel',$session);
					$this->usuario->save($resultado['id'],array("ultimo_acceso"=>date('Y-m-d H:i:s')));
					$ret['estado'] = true;
				}
				else
					$ret['msj'] = "Hola ".$resultado['nombre']." ".$resultado['apellido'].", tu cuenta se encuentra deshabilitada. Comunicate con el administrador del panel.";
			}
			else
				$ret['msj']="Los datos ingresados no son correctos. Intentá nuevamente.";
		}
		else
			$ret['msj'] = "Tenes que ingresar tu correo y contraseña.";

		echo json_encode($ret);

	}

	public function cerrar_sesion()
	{
		
		$user=$this->session->userdata("usrpanel");
		$this->session->sess_destroy();
		header('location:'.site_url());
	}

	public function eliminar()
	{
		$user = $this->session->userdata("usrpanel");
			if ($user['usuario_id']>0)
			{
				$this->load->model("usuario","usuario",true);
				$permisos=$this->usuario->getPermisos($user['perfil_id'],$this->pagina);
				if($permisos['baja']==1)
				{
					$id=$this->input->post("id");
					$this->load->model("usuario","usuario",true);
					$resultado=$this->usuario->delete($id);
					if($resultado==true)
						echo '1';
					else
						echo '2';
				}
				else
					echo '3';
			}
			else
				header('location:'.site_url());
	}

	public function recordar_pass()
	{
		$user = $this->session->userdata("usrpanel");
		if (!$user)
		{
			$email = $this->input->post("email");
			if (valid_email($email))
			{
				$this->load->model("usuario","usuario",true);
				if ($us=$this->usuario->getUsuarioEmail($email))
				{
					$pass_nuevo = rand(10000,99999);
					$datos['password'] = do_hash($pass_nuevo,'md5');
					if ($this->usuario->update($us['id'],$datos))
					{
						$config_email['smtp_host'] = HOST_MAIL;
						$config_email['smtp_user'] = USER_MAIL;
						$config_email['smtp_pass'] = PASS_MAIL;
						$config_email['smtp_port'] = PUERTO_MAIL;
						$config_email['mailtype'] = 'html';
						$config_email['protocol'] = 'smtp';

						$this->load->library("email",$config_email);
						$nombre = $us['nombre']." ".$us['apellido'];
						$this->email->from(EMAIL_SENDER,"Panel");
						$this->email->to($us['email']);
						$this->email->subject("Nueva contraseña");
						$this->email->message("Hola ".$nombre.".\n<br> Hemos resetado tu contrase&ntilde;a para que puedas ingresar. Record&aacute; modificarla como primera acci&oacute;n por una personal que recuerdes.\n\n<br><br>
							Email: ".$us['email']."\n<br>Contrase&ntilde;a: ".$pass_nuevo."\n\n<br><br>Link al panel: <a href='".site_url()."'>".site_url()."</a>");
						if ($this->email->send())
						{
							$res['res'] = "ok";
							$res['msj'] = "Recibiras un email con la nueva contrase&ntilde;a para poder acceder.";
						}
						else
						{
							$res['res'] = "error_envio";
							$res['msj'] = "Se produjo un error inesperado. Intentalo nuevamente.";
						}
					}
					else
					{
						$res['res'] = "error_conexion";
						$res['msj'] = "La conexi&oacute;n se ha perdido.";
					}
				}
				else
				{
					$res['res'] = "error_email_noexiste";
					$res['msj'] = "Email inexistente.";
				}
			}
			else
			{
				$res['res'] = "error_email";
				$res['msj'] = "Email incorrecto.";
			}
			echo json_encode($res);
		}
		else
			header("location: ".site_url(""));
	}
}
?>