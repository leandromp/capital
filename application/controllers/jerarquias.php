<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Jerarquias extends CI_Controller{

  public function __construct()
  {
    parent::__construct();
    $this->pagina=$this->uri->segment(1);
    $this->load->model('jerarquia','jerarquia',true);
  }

  public function _vistas($variables="")
	{
		$user=$this->session->userdata("usrpanel");
		if($user['usuario_id']>0)
		{
			$variables['usuario'] = $user;
			$this->load->library('menu');
			$variables['menu'] = $this->menu->dame_menu();
      $variables['nombre_pagina'] = $this->pagina;
			if ($variables['permiso'])
			{
				$this->load->view('jerarquias/listado',$variables);
			}
			elseif($variables['vista']=='form-inc' and $user['usuario_id']==$variables['user']['id'])
			{
				$this->load->view('jerarquias/listado',$variables);
			}
			else
			{
				$variables['error'] = 'No tiene permisos para acceder a esta modulo';
				$this->load->view('dashboard/inicio',$variables);
			}
		}
		else
			header("location: ".site_url())	;
	}

	public function index($variables="")
	{
		$user=$this->session->userdata("usrpanel");
		if($user['usuario_id']>0)
		{
			$this->load->model("usuario","usuario",true);
			$permiso = $this->usuario->getPermisos($user['perfil_id'],$this->pagina);
			$variables['permiso'] = $permiso['listado'];
			$variables['niveles'] = $this->jerarquia->getListado();
			$this->_vistas($variables);
		}
		else
			header("location:".site_url());
	}

	public function alta()
	{
		$user=$this->session->userdata("usrpanel");
		if($user['usuario_id']>0)
		{
			$this->load->model("usuario","usuario",true);
			$permiso = $this->usuario->getPermisos($user['perfil_id'],$this->pagina);
			$variables['permiso'] = $permiso['alta'];
			$variables['perfiles'] =  $this->usuario->getPerfiles();
			$variables['vista']="form-inc";
			$this->_vistas($variables);
		}
		else
			header("location:".site_url());
	}

  public function editar($id=0)
  {
    $user=$this->session->userdata("usrpanel");
    if($user['usuario_id']>0)
    {
      $this->load->model("usuario","usuario",true);
      $permiso = $this->usuario->getPermisos($user['perfil_id'],$this->pagina);
      $variables['permiso'] = $permiso['mod'];
      $variables['perfiles'] =  $this->usuario->getPerfiles();
      $variables['vista']="form-inc";
      $variables['nivel']=$this->jerarquia->getJerarquiaById($id);
      $this->_vistas($variables);
    }
    else
      header("location:".site_url());
  }

  public function guardar()
  {

    $user=$this->session->userdata("usrpanel");
    $res['estado'] = 0;
    if($user['usuario_id']>0)
    {
      $this->load->model("usuario","usuario",true);
      $permisos=$this->usuario->getPermisos($user['perfil_id'],$this->pagina);
      $id=$this->input->post("id");
      $permiso = ($id)?$permisos['mod']:$permisos['alta'];
      if ($permiso or $user['usuario_id']==$id)
      {
        $datos["descripcion"] = $this->input->post("descripcion");
        $datos["valor"] = $this->input->post("valor");

        if ($this->jerarquia->save($id,$datos))
        {
            $res['estado'] = 1;
            $res['msj'] = 'La operación fue realizada con éxito.';
        }
        else
          $res['msj'] = 'No se pudo finalizar la operación solicitada. Asegurate estar conectado a Internet.';

      }
      else
        $res['msj'] = "No tenes permiso para realizar la operación solicitada.";
    }
    else
      $res['msj'] = "Tu sesión ha finalizado. Tenes que iniciarla nuevamente.";

    echo json_encode($res);
  }

  public function eliminar()
  {
    $user = $this->session->userdata("usrpanel");
      if ($user['usuario_id']>0)
      {
        $this->load->model("usuario","usuario",true);
        $permisos=$this->usuario->getPermisos($user['perfil_id'],$this->pagina);
        if($permisos['baja']==1)
        {
          $id=$this->input->post("id");
          $this->load->model("jerarquia","jerarquia",true);
          $resultado=$this->jerarquia->delete($id);
          if($resultado==true)
            echo '1';
          else
            echo '2';
        }
        else
          echo '3';
      }
      else
        header('location:'.site_url());
  }

}
?>
