<?
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
 
class Pdfs extends CI_Controller {
 
    function __construct() {
        parent::__construct();

        //$this->load->model('pdfs_model');
    }
    
    public function index()
    {
        $this->load->library('Pdf');
        //$data['provincias'] llena el select con las provincias españolas
        //$data['provincias'] = $this->pdfs_model->getProvincias();
        //cargamos la vista y pasamos el array $data['provincias'] para su uso
        //$this->load->view('pdfs_view', $data);
        //echo dirname(__FILE__);
    }

    public function cantidad_departamentos()
    {
        $this->load->model('departamento','departamento',true);
        
        $resultado = count($this->departamento->getListado());
        $datos= '<h1> Cantidad de departamentos en la organización: '.$resultado;
        $this->generar_reporte_pdf($datos);

    }

    public function cantidad_puestos_departamentos()
    {
        $this->load->model('departamento','departamento',true);
        $departamentos = $this->departamento->getListado();
        foreach ($departamentos as $key => $dep) {
            $departamentos[$key]['cantidad_puestos'] = $this->departamento->getCantidadPuestoByDepartamentoId($dep['id']);
        }
        $resultado['departamentos'] = $departamentos;
        $datos=$this->load->view('dashboard/puestos_x_departamentos',$resultado,true);
        $this->generar_reporte_pdf($datos);
    }

    public function puestos_departamentos()
    {
        $this->load->model('departamento','departamento',true);
        $departamentos = $this->departamento->getPuestoByDepartamentoId();
        $resultado['departamentos'] = $departamentos;
        $datos=$this->load->view('dashboard/puestos_departamento_detalle',$resultado,true);
        $this->generar_reporte_pdf($datos);
    }

    public function puestos_departamentos_bis()
    {
        $this->load->model('departamento','departamento',true);
        $departamentos = $this->departamento->getPuestoByDepartamento();
        $resultado['departamentos'] = $departamentos;
        $datos=$this->load->view('dashboard/puestos_departamento_detalle',$resultado,true);
        $this->generar_reporte_pdf($datos);
    }

     public function cantidad_empleados_departamentos()
    {

        $this->load->model('departamento','departamento',true);
        $this->load->model('empleado','empleado',true);
        $departamentos = $this->departamento->getListado();
        foreach ($departamentos as $key => $dep) {
            $departamentos[$key]['cantidad_puestos'] = $this->departamento->getCantidadEmpleadosByDepartamentoId($dep['id']);
        }
        $resultado['departamentos'] = $departamentos;
        $resultado['cantidad_empleados'] = count($this->empleado->getListado());
        $datos=$this->load->view('dashboard/empleados_x_departamentos',$resultado,true);
        $this->generar_reporte_pdf($datos);
    }

    public function departamentos_nivel()
    {
        $this->load->model('departamento','departamento',true);
        $departamentos = $this->departamento->getDepartamentosByNivel();
        $resultado['departamentos'] = $departamentos;
        $datos=$this->load->view('dashboard/departamentos_nivel',$resultado,true);
        $this->generar_reporte_pdf($datos);
    }
    
    public function responsable_nivel()
    {
        $this->load->model('departamento','departamento',true);
        $departamentos = $this->departamento->getResponsableNivel();
        $resultado['departamentos'] = $departamentos;
        $datos=$this->load->view('dashboard/responsable_departamento',$resultado,true);
        $this->generar_reporte_pdf($datos);
    }

    public function empleados_dos_puestos()
    {
        $this->load->model('departamento','departamento',true);
        $empleados = $this->departamento->getEmpleadosActuales();
        $resultado=array();

        foreach ($empleados as $key => $value) {
            $temp = $this->departamento->getEmpleadosMasPuestos($value['empleado_id']);
            
            $resultado=array_merge($resultado,$temp);

        }
        $datos['empleados']=$resultado;
        $datos=$this->load->view('dashboard/empleados_dos_puestos',$datos,true);
        $this->generar_reporte_pdf($datos);
    }

    public function listar_empleados_by_departamento($id_departamento)
    {
        $this->load->model('departamento','departamento',true);
        $empleados = $this->departamento->getEmpleadosByDeptoId($id_departamento);
        $datos['empleados']=$empleados;
        $datos=$this->load->view('dashboard/empleados_dos_puestos',$datos,true);
        $this->generar_reporte_pdf($datos);
    }



    public function generar_reporte_pdf($datos) {
        $this->load->library('Pdf');
        
        $pdf = new Pdf('P', 'mm', 'A4', true, 'UTF-8', false);
        $pdf->SetCreator(PDF_CREATOR);
        $pdf->SetAuthor('Israel Parra');
        $pdf->SetTitle('Reporte incidencias ');
        $pdf->SetSubject('Tutorial TCPDF');
        $pdf->SetKeywords('TCPDF, PDF, example, test, guide');
 
// datos por defecto de cabecera, se pueden modificar en el archivo tcpdf_config_alt.php de libraries/config
        $pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE . ' 001', PDF_HEADER_STRING, array(0, 64, 255), array(0, 64, 128));
        $pdf->setFooterData($tc = array(0, 64, 0), $lc = array(0, 64, 128));
 
// datos por defecto de cabecera, se pueden modificar en el archivo tcpdf_config.php de libraries/config
        $pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
        $pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
 
// se pueden modificar en el archivo tcpdf_config.php de libraries/config
        $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
 
// se pueden modificar en el archivo tcpdf_config.php de libraries/config
        $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
        $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
        $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
 
// se pueden modificar en el archivo tcpdf_config.php de libraries/config
        $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
 
//relación utilizada para ajustar la conversión de los píxeles
        $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
 
 
// ---------------------------------------------------------
// establecer el modo de fuente por defecto
        $pdf->setFontSubsetting(true);
 
// Establecer el tipo de letra
 
//Si tienes que imprimir carácteres ASCII estándar, puede utilizar las fuentes básicas como
// Helvetica para reducir el tamaño del archivo.
        $pdf->SetFont('freemono', '', 14, '', true);
 
// Añadir una página
// Este método tiene varias opciones, consulta la documentación para más información.
        $pdf->AddPage('L','A4');
 
//fijar efecto de sombra en el texto
        $pdf->setTextShadow(array('enabled' => true, 'depth_w' => 0.2, 'depth_h' => 0.2, 'color' => array(196, 196, 196), 'opacity' => 1, 'blend_mode' => 'Normal'));
 
// Establecemos el contenido para imprimir
        $html.=$datos;
 
// Imprimimos el texto con writeHTMLCell()
        $pdf->writeHTMLCell($w = 0, $h = 0, $x = '', $y = '', $html, $border = 0, $ln = 1, $fill = 0, $reseth = true, $align = '', $autopadding = true);
 
// ---------------------------------------------------------
// Cerrar el documento PDF y preparamos la salida
// Este método tiene varias opciones, consulte la documentación para más información.
        $nombre_archivo = utf8_decode("Localidades de ".$prov.".pdf");
        $pdf->Output($nombre_archivo, 'I');
    }
}