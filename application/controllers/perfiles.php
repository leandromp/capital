<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	
	class Perfiles extends CI_Controller {
	
	function __construct()
	{
		parent::__construct();
		header ("Expires: Thu, 27 Mar 1980 23:59:00 GMT"); //la pagina $
        header ("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
        header ("Cache-Control: no-cache, must-revalidate"); //no guard$
        header ("Pragma: no-cache");
        $this->pagina = $this->uri->segment(1);
	}
	

	public function index($variables="")
	{
		$user=$this->session->userdata("usrpanel");

		if($user['usuario_id']>0)
		{
			$variables['usuario'] = $user;
			$this->load->model("usuario","usuario",true);
			$permisos = $this->usuario->getPermisos($user['perfil_id'],$this->pagina);
			$variables['menu'] = $this->menu->dame_menu();
			if($permisos['listado'])
			{
				$this->load->library('menu');
				$variables['nombre_pagina'] = $this->pagina;
				$this->load->model('perfil');
				$variables['perfiles'] = $this->perfil->getPerfiles($user['perfil_codigo']);
				$this->load->view('perfiles/listado',$variables);
			}
			else
			{
				$variables['error'] = 'No tiene permisos para acceder a esta modulo';
				$this->load->view('dashboard/inicio',$variables);
			}
			
		}
		else
			header('location:'.site_url());
		
	}

	public function alta($id=0)
	{
		$user=$this->session->userdata("usrpanel");
		if($user['usuario_id']>0)
		{
			$this->load->model("usuario","usuario",true);
			$permisos = $this->usuario->getPermisos($user['perfil_id'],$this->pagina);
			if($permisos['alta']==1)
			{
				if($id!=0)
				{
					
					$variables['mensaje']="";
				}
				else
				{
					$error=0;
					//$datos["marca"] = $this->input->post("marca");
					$datos["nombre"] = $this->input->post("nombre");
					$datos["codigo"] = $this->input->post("codigo");
					//controlo que los datos no vengan vacios
					foreach ($datos as $key => $value) 
					{
						if($value=="")
							$error=1;
					}						
				
					$this->load->model("perfil","perfil",true);
					if($error!=1) //si los datos no vienen vacios entonces intento hacer el insert en la DB
					{
						$resultado=$this->perfil->insert($datos);
						if($resultado==true)
						$variables['mensaje'] = 'Los datos fueron ingresados correctamente';
						else
						$variables['error'] = 'hubo un error al intentar actualizar la Base de Datos';		
					}
					elseif ($error>=1) {
							$variables['error'] = "Debe ingresar todos los campos necesearios";
					}	
					else
						$variables['error'] = '';
					
				}
				
				$variables['vista']="form-inc";
				$this->index($variables);
			}
			else
			{
				$variables['error']= 'no tiene permisos para usar esta Funcion';
				$this->index($variables);
			}
			 
		}
	}

	public function editar($id=0)
	{
		$user=$this->session->userdata("usrpanel");
			if($user['usuario_id']>0)
			{
				$this->load->model("usuario","usuario",true);
				$permisos=$this->usuario->getPermisos($user['perfil_id'],$this->pagina);
				if($permisos['mod']==1)
				{
					
						if($id==0) //si el id viene por post
						{
							$error=0;
							$id=$this->input->post("id");
							//$datos["marca"] = $this->input->post("marca");
							$datos["nombre"] = $this->input->post("nombre");
							$datos["codigo"] = $this->input->post("codigo");
							
							//controlo que los datos necesario no vengan vacios
							foreach ($datos as $key => $value) 
							{
								if($value=="")
									$error=1;
							}						

							$this->load->model("perfil","perfil",true);

							if($error!=1) //si los datos no vienen vacios entonces intento hacer el update en la DB
							{
								$resultado=$this->perfil->update($id,$datos);
								if($resultado==true)
								$variables['mensaje'] = 'Los datos fueron ingresados correctamente';
								else
								$variables['error'] = 'hubo un error al intentar actualizar la Base de Datos';		
							}
							else
								$variables['error'] = 'debe ingresar los datos necesarios para dar de alta al producto';


						}
						else //sino viene por url entonces completo los campos del formulario con los datos del cliente
						{

							$this->load->model("perfil","perfil",true);
							$variables['perfil']=$this->perfil->getPerfilId($id);
						}

						$variables['vista']="form-inc"; //por cualquiera sea listado o modificacion muestro el formulario y paso un mensaje.
						
				}
				else
					$variables['mensaje']="No tiene permisos para acceder a esta funcion";


				$this->index($variables);
			}
	}

	public function eliminar()
	{
		$user = $this->session->userdata("usrpanel");
			if ($user['usuario_id']>0)
			{
				$this->load->model("usuario","usuario",true);
				$permisos=$this->usuario->getPermisos($user['perfil_id'],$this->pagina);
				if($permisos['baja']==1)
				{
					$id=$this->input->post("id");
					$this->load->model("perfil","perfil",true);
					$resultado=$this->perfil->delete($id);
					if($resultado==true)
						echo '1';
					else
						echo '2';
				}
				else
					echo '3';
			}
			else
				header('location:'.site_url());
	}
}
	
	/* End of file usuarios.php */
	/* Location: ./application/controllers/usuarios.php */
?>