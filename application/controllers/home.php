<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	 
class Home extends CI_Controller {
	 
 	public function index()
 	{
 		$user = $this->session->userdata("usrpanel");
 		if ($user['usuario_id']>0)
 		{ 			
 			$variables['usuario'] = $user;
 			$this->load->library('menu');
 			$variables['menu'] = $this->menu->dame_menu();
 			$this->load->view('dashboard/inicio_blanco',$variables);	
 		}
 		else
 			$this->load->view('login');
 	}
}
?>