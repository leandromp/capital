<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Secciones extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		header ("Expires: Thu, 27 Mar 1980 23:59:00 GMT");
        header ("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
        header ("Cache-Control: no-cache, must-revalidate");
        header ("Pragma: no-cache");
        $this->pagina = $this->uri->segment(1);
	}

	public function _vistas($variables="")
	{
		$user=$this->session->userdata("usrpanel");
		if($user['usuario_id']>0)
		{
			$variables['usuario'] = $user;
			$this->load->library('menu');
			$variables['menu'] = $this->menu->dame_menu();
			$variables['nombre_pagina'] = $this->pagina;
			if ($variables['permiso'])
			{
				$this->load->view('secciones/listado',$variables);
			}
			else
			{
				$variables['error'] = 'No tiene permisos para acceder a esta modulo';
				$this->load->view('dashboard/listado',$variables);
			}
		}
		else
			header("location: ".site_url())	;
	}

	public function index($variables="")
	{
		$user=$this->session->userdata("usrpanel");
		if($user['usuario_id']>0)
		{
			$variables['usuario'] = $user;
			$this->load->model('seccion','seccion',TRUE);
			$this->load->library('menu');
			$variables['menu'] = $this->menu->dame_menu();
			$variables['nombre_pagina'] = $this->pagina;
			$variables['secciones'] = $this->seccion->getListado();
			$this->load->view('secciones/listado',$variables);
		}
		else
			header("location:".site_url());
	}

	public function guardar()
	{
		$user=$this->session->userdata("usrpanel");
		$res['estado'] = 0;
		if($user['usuario_id']>0)
		{
			$this->load->model("usuario","usuario",true);
			$permisos=$this->usuario->getPermisos($user['perfil_id'],$this->pagina);
			$id=$this->input->post("id");
			$permiso = ($id)?$permisos['mod']:$permisos['alta'];
			if ($permiso)
			{
				$id=$this->input->post("id");
				$datos['nombre'] = $this->input->post("nombre");

				foreach ($datos as $key => $value) 
				{
					if(!$value)
					{
						$res['msj'] = 'Tenes que ingresar toda la información solicitada';
						echo json_encode($res);
						die();
					}
				}
				if($this->input->post('habilitada')=='on')
							$datos['habilitada']=1;
						else
							$datos['habilitada']=0;
				$this->load->model("seccion","seccion",true);
				if ($this->seccion->save($id,$datos))
				{
					$res['estado'] = 1;
					$res['msj'] = 'La operación fue realizada con éxito.';
				}
				else
					$res['msj'] = 'No se pudo finalizar la operación solicitada. Asegurate estar conectado a Internet.';
			}
			else
				$res['msj'] = "No tenes permiso para realizar la operación solicitada.";
		}
		else
			$res['msj'] = "Tu sesión ha finalizado. Tenes que iniciarla nuevamente.";

		echo json_encode($res);
	}

	public function eliminar()
	{
	$user = $this->session->userdata("usrpanel");
		if ($user['usuario_id']>0)
		{
			$this->load->model("usuario","usuario",true);
			$permisos=$this->usuario->getPermisos($user['perfil_id'],$this->pagina);
			if($permisos['baja']==1)
			{
				$this->load->model("usuario","usuario",true);
				$permisos=$this->usuario->getPermisos($user['perfil_id'],$this->pagina);
				if($permisos['baja']==1)
				{
					$id=$this->input->post("id");
					$this->load->model("seccion","seccion",true);
					$resultado=$this->seccion->delete($id);
					if($resultado==true)
						echo '1';
					else
						echo '2';
				}
				else
					echo '3';
			}
			else
				echo 'no tiene permisos';
			
		}
		else
			header('location:'.site_url());
	}
}

/* End of file secciones.php */
/* Location: ./application/controllers/secciones.php */