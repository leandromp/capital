<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Empleados extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		header ("Expires: Thu, 27 Mar 1980 23:59:00 GMT");
        header ("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
        header ("Cache-Control: no-cache, must-revalidate");
        header ("Pragma: no-cache");
        $this->pagina = $this->uri->segment(1);
			  $this->load->model("empleado","empleado",true);
	}

	public function _vistas($variables="")
	{
		$user=$this->session->userdata("usrpanel");
		if($user['usuario_id']>0)
		{
			$variables['usuario'] = $user;
			$this->load->library('menu');
			$variables['menu'] = $this->menu->dame_menu();
			$variables['nombre_pagina'] = $this->pagina;
			$variables['usuarios'] = $this->usuario->getUsuarios();
			if ($variables['permiso'])
			{
				$this->load->view('empleados/listado',$variables);
			}
			elseif($variables['vista']=='form-inc' and $user['usuario_id']==$variables['user']['id'])
			{
				$this->load->view('empleados/listado',$variables);
			}
			else
			{
				$variables['error'] = 'No tiene permisos para acceder a esta modulo';
				$this->load->view('dashboard/inicio',$variables);
			}
		}
		else
			header("location: ".site_url())	;
	}

	public function index($variables="")
	{
		$user=$this->session->userdata("usrpanel");
		if($user['usuario_id']>0)
		{
			$this->load->model("usuario","usuario",true);
			$permiso = $this->usuario->getPermisos($user['perfil_id'],$this->pagina);
			$variables['permiso'] = $permiso['listado'];
			$variables['empleados'] = $this->empleado->getListado();
			$this->_vistas($variables);
		}
		else
			header("location:".site_url());
	}

	public function alta()
	{
		$user=$this->session->userdata("usrpanel");
		if($user['usuario_id']>0)
		{
			$this->load->model("usuario","usuario",true);
			$permiso = $this->usuario->getPermisos($user['perfil_id'],$this->pagina);
			$variables['permiso'] = $permiso['alta'];
			$variables['vista']="form-inc";
			$this->_vistas($variables);
		}
		else
			header("location:".site_url());
	}

	public function editar($id=0)
	{
		$user=$this->session->userdata("usrpanel");
		if($user['usuario_id']>0)
		{
			$this->load->model("usuario","usuario",true);
			$permiso = $this->usuario->getPermisos($user['perfil_id'],$this->pagina);
			$variables['permiso'] = $permiso['mod'];
			$variables['vista']="form-inc";
			$variables['user']=$this->empleado->getEmpleadoId($id);
			$this->_vistas($variables);
		}
		else
			header("location:".site_url());
	}


		public function puesto($id=0)
		{
			$user=$this->session->userdata("usrpanel");
			if($user['usuario_id']>0)
			{
				$this->load->library('varios_library');
				$this->load->model("usuario","usuario",true);
				$permiso = $this->usuario->getPermisos($user['perfil_id'],$this->pagina);
				$variables['permiso'] = $permiso['mod'];
				$variables['vista']="puestos";
				$variables['empleado_id']=$id;
				$variables['puestos'] = $this->empleado->getPuestos();
				$variables['instancias'] = $this->empleado->getInstancias($id);
				 
				if (count($variables['instancias'])>1)
				{
					$cantidad=count($variables['instancias']);
					$i=1;
					foreach ($variables['instancias'] as $key => $value) {
							$fecha_inicio = $value['fecha'];
							if($i<=$cantidad)
								{
									$fecha_fin = $variables['instancias'][$key+1]['fecha'];
									if($i==$cantidad)
										$fecha_fin = date('Y-m-d');

									$resultado = $this->empleado->getHistoricoByFechas($fecha_inicio,$fecha_fin,$id);
									$resul[$key] = array_merge($variables['instancias'][$key],$resultado[0]);
									$variables['historico'] = $resul;
								}
						
							$i++;
						}	
				}
				else
					$variables['historico'] = $this->empleado->getHistoricoUnico($id);


				//echo '<pre>'.print_r($variables['historico'],true);
				$this->_vistas($variables);
			}
			else
				header("location:".site_url());
		}

	public function guardar()
	{
		$user=$this->session->userdata("usrpanel");
		$res['estado'] = 0;
		if($user['usuario_id']>0)
		{
			$this->load->model("usuario","usuario",true);
			$permisos=$this->usuario->getPermisos($user['perfil_id'],$this->pagina);
			$id=$this->input->post("id");
			$permiso = ($id)?$permisos['mod']:$permisos['alta'];

			if ($permiso or $user['usuario_id']==$id)
			{
				$datos["nombre"] = $this->input->post("nombre");
				$datos["dni"] = $this->input->post("dni");
				$datos["legajo"] = $this->input->post("legajo");
				$datos["id_usuario"] = $this->input->post("id_usuario");
				if (!$permiso)
					unset($datos["perfil_id"]);

					foreach ($datos as $key => $value)
					{
						if(!$value)
						{
								$res['msj'] = 'Tenes que ingresar toda la información solicitada';
								echo json_encode($res);
								die();
						}
					}


					$datos["habilitado"] = $this->input->post("habilitado");
					if($datos['habilitado'])
						$datos['habilitado']=1;
					else
						$datos['habilitado']=0;

					if(!$id)
						$datos['fecha_alta']=date('Y-m-d');


					if ($this->empleado->save($id,$datos))
					{
						$res['estado'] = 1;
						$res['msj'] = 'La operación fue realizada con éxito.';

						if ($id == $user['id'])
						{
							$user_ses = $this->session->userdata('usrpanel');
							$user_ses['nombre'] = $datos['nombre'];
							$user_ses['apellido'] = $datos['apellido'];
							$user_ses['email'] = $datos['email'];
							$this->session->set_userdata('usrpanel',$user_ses);
						}
					}
					else
						$res['msj'] = 'No se pudo finalizar la operación solicitada. Asegurate estar conectado a Internet.';
				}

			else
				$res['msj'] = "No tenes permiso para realizar la operación solicitada.";
		}
		else
			$res['msj'] = "Tu sesión ha finalizado. Tenes que iniciarla nuevamente.";

		echo json_encode($res);
	}

	public function eliminar()
	{
		$user = $this->session->userdata("usrpanel");
			if ($user['usuario_id']>0)
			{
				$this->load->model("usuario","usuario",true);
				$permisos=$this->usuario->getPermisos($user['perfil_id'],$this->pagina);
				if($permisos['baja']==1)
				{
					$id=$this->input->post("id");
					$this->load->model("empleado","empleado",true);
					$resultado=$this->empleado->delete($id);
					if($resultado==true)
						echo '1';
					else
						echo '2';
				}
				else
					echo '3';
			}
			else
				header('location:'.site_url());
	}

	public function agregar_puesto()
	{
		
		$datos['empleado_id'] = $this->input->post('empleado_id');
		$datos['fecha'] = $this->input->post('fecha');
		$datos['puesto_id'] = $this->input->post('puesto_id');
		$actual = $this->input->post('actual');
		if($actual=="true")
			$datos['actual']=1;
		else
			$datos['actual']=0;

		$control = $this->empleado->noAsignado($datos['empleado_id'],$datos['puesto_id']);
		if($control==1)
			$respuesta = $this->empleado->addPuesto($datos);
		else
			echo 'ko';

	}
}
?>
