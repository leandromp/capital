<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	
class Modulos extends CI_Controller 
{

	function __construct()
	{
		parent::__construct();
		header ("Expires: Thu, 27 Mar 1980 23:59:00 GMT"); //la pagina $
        header ("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
        header ("Cache-Control: no-cache, must-revalidate"); //no guard$
        header ("Pragma: no-cache");
        $this->pagina = $this->uri->segment(1);
	}
	
	public function index($variables="")
	{
		$user=$this->session->userdata("usrpanel");
		if($user['usuario_id']>0)
		{
			$variables['usuario'] = $user;
			$this->load->model('modulo','modulo',TRUE);
			$this->load->library('menu');
			$variables['menu'] = $this->menu->dame_menu();
			$variables['nombre_pagina'] = $this->pagina;
			$variables['modulos'] = $this->modulo->getAll();
			$this->load->view('modulos/listado',$variables);
		}
		else
		{
			$variables['error'] = 'No tiene permisos para acceder a esta modulo';
			$this->load->view('dashboard/listado',$variables);
		}
		
	}

		public function alta($id=0)
		{
			$user = $this->session->userdata('usrpanel');
			if($user['usuario_id']>0)
			{
				$this->load->model('usuario','usuario',true);
				$permisos = $this->usuario->getPermisos($user['perfil_id'],$this->pagina);
				if ($permisos['alta'])
				{
					$datos['titulo'] = $this->input->post("titulo");
					$datos['accion'] = $this->input->post("accion");
					$datos['icono'] = $this->input->post("icono");
					$datos['padre_id'] = $this->input->post("padre_id");
					$datos['orden'] = $this->input->post("orden");
					$datos['hijos'] = $this->input->post("hijos");

					if ($datos['titulo']!="" and $datos['accion']!="")
					{
						$this->load->model('modulo','modulo',true);
						$modulo_id=$this->modulo->insert($datos);
						//cargo todos los perfiles para hacer un insert por cada uno en la tabla 
						//permisos pero con los campos en 0
						$this->load->model("permiso","permiso",true);
						$temp = explode("/", $datos['accion']);
						$accion = $temp[0];
						$perfiles=$this->permiso->getPerfiles();
						foreach ($perfiles as $key => $value) 
						{
							if ($this->permiso->insertEmpty($modulo_id,$accion,$value['id']))
								$variables['mensaje'] = 'Datos Actualizados correctamente';
							else
								$variables['mensaje'] = "Hubo un error al intentar actulizar comuniquelo al Administrador. Gracias.";

						}
					}
					else
					{  
						if($alert==0)	//debo ocultar las alertas
							$variables['error']="Faltan los datos requeridos para poder dar de alta el modulo";
					}

				}
				else
					$variables['error']="No tiene los permisos suficientes para acceder a este modulo";

				$variables['vista']='form-inc';
				$this->index($variables);
			}
			else
				// no hay ningun usuario logeado
				header('location:'.site_url());
		}

		public function editar($id=0)
		{
			$user = $this->session->userdata("usrpanel");
			if ($user['usuario_id']>0)
			{
				$this->load->model('usuario','usuario',true);
				$permisos = $this->usuario->getPermisos($user['perfil_id'],$this->pagina);
				if ($permisos['mod'])
				{
					$this->load->model("modulo","modulo",true);
					if ($id==0)//el formulario envio el id por post y esta intentando realizar un update
					{
						$id=$this->input->post("id");
						$datos['titulo'] = $this->input->post("titulo");
						$datos['accion'] = $this->input->post("accion");
						$datos['icono'] = $this->input->post("icono");
						$datos['padre_id'] = $this->input->post("padre");
						$datos['hijos'] = $this->input->post("hijos");
						$datos['orden'] = $this->input->post("orden");
						if($datos['hijos']=='on')
							$datos['hijos']=1;
						else
							$datos['hijos']=0;
						if ($datos["titulo"]!="" and $datos["accion"]!="")
						{
							if($this->modulo->update($id,$datos))
								$variables['mensaje']="Modulo modificado correctamente";
							else
								$variables['error'] = "Hubo un error al intentar actulizar comuniquelo al Administrador. Gracias.";
						}
						else
						{
							$datos['id']=$id;
							$variables['modulo']=$datos;
							$variables['mensaje']="Falta los datos requeridos para poder modificar el modulo";
						}
					}
					else //sino es que quiero ver los datos en el formulario y paso el id del modulo por url
					{
						$resultado=$this->modulo->getModulo($id);	
						$variables['modulo']=$resultado;
					}
				}
				else
					$variables['error'] = 'No tiene permisos para acceder a esta modulo.';

				$variables['vista']='form-inc';
				$this->index($variables);
			}				
		}

		public function eliminar()
		{
			$user = $this->session->userdata("usrpanel");
			if ($user['usuario_id']>0)
			{
				$id=$this->input->post("id");
				$this->load->model("modulo","modulo",true);
				$resultado=$this->modulo->delete($id);
				if($resultado==true)
					echo '1';
				else
					echo '2';
			}
			else
				header('location:'.site_url());
		}

		public function permisos($update=0)
		{
			$user = $this->session->userdata("usrpanel");
			$this->load->model('usuario','usuario',true);
			$permisos = $this->usuario->getPermisos($user['perfil_id'],$this->pagina);
			if ($permisos['mod'])
			{
				$this->load->model("modulo","modulo",true);
				$this->load->model("usuario","usuario",true);
				$usuario_id=$user['usuario_id'];
				$perfiles = $this->usuario->getPerfiles();
				if($update==0)
				{
					$resultado = $this->modulo->listado($user['usuario_id']);
					$variables['permisos'] = $resultado;
					$variables['perfiles']= $perfiles;
					//$variables['perfil_activo'] = $user[]
					$variables['vista']='permisos-inc';
					$this->index($variables);
				}
				else
				{
					$perfil_id=$this->input->post("perfil_id");
					if($perfil_id>0)//corroboro que el perfil de usuario exista;
					{
						//obtengo la cantidad de modulos del sistema
						$modulos=$this->modulo->getAll();
						$this->load->model("permiso","permiso",true);
						foreach ($modulos as $key => $value) {
							//echo 'modulo_id es'.$value['id'];
						 	for ($i=1; $i < 5 ; $i++) { 
						 		$value['id'].'-'.$i;
						 		$columna=array(1=>'alta',2=>'baja',3=>'mod',4=>'listado');
						 		$opcion=$this->input->post($value['id'].'-'.$i);

						 		if($opcion=='on')
						 			$valor_update=1;
						 		else
						 			$valor_update=0;

						 		if(!$this->permiso->updateSimple($value['id'],$perfil_id,$columna[$i],$valor_update))
						 			$variables['error']= 'lo sentimos hubo un error al actulizar la base de datos.';
						 		else
						 			$variables['mensaje']="Modificaciones Guardadas Correctamente";
						 			
							 	}
							 $variables['perfiles']=$perfiles;
							 $variables['permisos']=$this->modulo->listado($user['perfil_id']);;
						}
					}
					else
					{
						$variables['permisos'] = "";

						$variables['error']="no ha seleccionado ningun perfil de usuario";
					}
					

					$variables['vista']="permisos-inc";
					$this->index($variables);
				}
				
				
			}
			else
			{
				echo 'no tiene permisos para acceder al modulo';
			}
		}

		public function cambiar_perfil()
		{
			$perfil_id=$this->input->post("perfil");
			if($perfil_id>0)
			{
				$this->load->model("usuario","usuario",true);
				//$perfiles = $this->empleado->getPerfiles();
				//$variables['perfiles']=$perfiles;
				// arriba vuelvo a cargar los perfiles
				$this->load->model("modulo","modulo",true);
				$variables['perfil_seleccionado']=$perfil_id;
				$variables['permisos'] = $this->modulo->listado($perfil_id);
				$vista=$this->load->view("modulos/listado-permisos-inc",$variables,true);
				echo $vista;	
			}
			else
				$mensaje='No ah seleccionado ningun perfil.';
		}

		public function listar_modulos($perfil_id)
		{
			$perfil_id=$this->input->post("perfil_id");
			if($perfil_id>0)
			{
				//$perfil_id=$this->input->post("perfil_id"); //este es el que viene del formulario no el que se usa para controlar si esta logeado un usuario.
				$this->load->model('modulo','modulo',true);
				$resultado = $this->modulo->getModulosNa($perfil_id); //Na = no asignados al perfil
				if(count($resultado)>0)
				{
					$elemento = '<label> Seleccione el tipo de Modulo </label> 
					<select id="modulo" class="form-control">';
					foreach ($resultado as $res)
					{
					$elemento.="<option value=".$res['id'].">".$res['titulo']." </option>";
					}
					$elemento.="</select>";	
				}
				else
					$elemento =1;
				
				echo $elemento;
			}
			else
				echo '2';
		}

		public function agregar_modulo_perfil()
		{
			$user = $this->session->userdata("usrpanel");
			if ($user['perfil_id']>0)
			{	
				$perfil_id = $this->input->post("perfil_id");
				$this->load->model('usuario','usuario',true);
				$permisos = $this->usuario->getPermisos($user['perfil_id'],$this->pagina);
				if ($permisos['alta'])
				{
					$datos['modulo_id'] = $this->input->post("modulo_id");
					$datos['perfil_id'] = $perfil_id;
					if($datos['modulo_id']>0 and $datos['perfil_id']>0)
					{
						$this->load->model('modulo','modulo',true);
						$accion = $this->modulo->getAccion($datos['modulo_id']);
						$temp = explode('/', $accion);
						$datos['accion'] = $temp[0];
						if($this->modulo->addModuloPerfil($datos))
							$variables['mensaje'] = 'Datos correctamente agregados';
						else
							$variables['error'] = 'Hubo un error comuniquelo al Administrador';

					}
					else
						echo '3'; //no existe al menos unos de los datos deberia dar una advertencia.
				}

				$variables['perfil_seleccionado']=$perfil_id;
				$variables['permisos'] = $this->modulo->listado($perfil_id);
				$vista=$this->load->view("modulos/listado-permisos-inc",$variables,true);
				echo $vista;	
			}
			else
			 	header('location:'.site_url());
				
		}

		public function limpiar_db()
		{
			$this->load->model('modulo','modulo',true);
			$this->modulo->limpiarDB();
			echo 'bye bye';
		}

}
	
	/* End of file modulo.php */
	/* Location: ./application/controllers/modulo.php */
?>
