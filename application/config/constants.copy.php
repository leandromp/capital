<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| File and Directory Modes
|--------------------------------------------------------------------------
|
| These prefs are used when checking and setting modes when working
| with the file system.  The defaults are fine on servers with proper
| security, but you may wish (or even need) to change the values in
| certain environments (Apache running a separate process for each
| user, PHP under CGI with Apache suEXEC, etc.).  Octal values should
| always be used to set the mode correctly.
|
*/
define('FILE_READ_MODE', 0644);
define('FILE_WRITE_MODE', 0666);
define('DIR_READ_MODE', 0755);
define('DIR_WRITE_MODE', 0777);

/*
|--------------------------------------------------------------------------
| File Stream Modes
|--------------------------------------------------------------------------
|
| These modes are used when working with fopen()/popen()
|
*/

define('FOPEN_READ', 							'rb');
define('FOPEN_READ_WRITE',						'r+b');
define('FOPEN_WRITE_CREATE_DESTRUCTIVE', 		'wb'); // truncates existing file data, use with care
define('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE', 	'w+b'); // truncates existing file data, use with care
define('FOPEN_WRITE_CREATE', 					'ab');
define('FOPEN_READ_WRITE_CREATE', 				'a+b');
define('FOPEN_WRITE_CREATE_STRICT', 			'xb');
define('FOPEN_READ_WRITE_CREATE_STRICT',		'x+b');


/*
|--------------------------------------------------------------------------
| Tama�os diferentes de las imagenes, URL base y Path base
|--------------------------------------------------------------------------
|
| These modes are used when working with fopen()/popen()
|
*/

define('PATH_BASE', "/var/www/capital/");
define('URL_BASE', "http://localhost/capital/");
define('URL_SITIO', "http://localhost/capital");
define('PATH_BASE_SITIO', "/var/www/capital/");

/*Cuenta SMTP*/
define('HOST_MAIL', "mail.hitmedia.com.ar");
define('USER_MAIL', "tucumanos+hitmedia.com.ar");
define('PASS_MAIL', "tucu2011");
define('PUERTO_MAIL', "26");
/**************/
/****** TAMAÑOS DE LAS IMAGNES****/
define('TAM_0_W',787); //tamaño home destacada central 787x254
define('TAM_1_W',330);
define('TAM_TH_W',40);
//define('TAM_LIST')

/*********************************/


/* End of file constants.php */
/* Location: ./system/application/config/constants.php */