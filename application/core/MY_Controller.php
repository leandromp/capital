<?
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class MY_Controller extends CI_Controller
{
	public function __construct()
    {
        parent::__construct();
    }

    public function _mi_ranking($id=0)
	{
		$user = $this->lg_api->get_data();
		if ($user)
		{						
			$rank=$this->_mi_rank(0);
			
			return $rank;
		}
	}
	
	public function _mi_rank($fase)
	{
		$user = $this->lg_api->get_data();
		$time_exp = 1200;
		
		if($user)
		{
			$participa=true;
		
			//ME FIJO SI ESTOY EN EL RANKING PRINCIPAL
			$k=1;
			$estoy_en_ranking=false;
			
			$ranking = $this->memcached_library->get("ranking_completo_".$fase);
			if(!$ranking){
				$this->load->model('prode','prode',true);
				$ranking = $this->prode->dameRankingFase($fase);
				$this->memcached_library->add("ranking_completo_".$fase,$ranking,$time_exp);
			}
			
			if ($ranking)
			{
				foreach($ranking as $r)
				{
					if($r['usuario_id']==$user['id']){
						$estoy_en_ranking=true;
						$posicion_actual=$k;
						$mis_puntos=$r['puntos'];
						break;
					}
					$k++;
				}

				if ($estoy_en_ranking)
				{
					$mi_ranking=array(
						'posicion'=>$posicion_actual,
						'usuario'=>$user['usuario'],
						'puntos'=>$mis_puntos
					);

					return $mi_ranking;
				}
				else
					return false;
			}
			else
				return false;
		}
		else
			return false;
	}

	public function _dameOpcionesResultados()
	{
		$this->load->library("memcached_library");
		$rsultados_posibles = $this->memcached_library->get('resultados_posibles_prode');
		if (!$resultados_posibles_prode)
		{
			$this->load->model("partido","partido",true);
			$resultados_posibles_prode = $this->partido->dameResultadosPosibles();
			$this->memcached_library->add('resultados_posibles_prode',$resultados_posibles_prode,72000);
		}

		return $resultados_posibles_prode;
	}

	public function _mensajes_pizarra($torneo_id)
	{
		$this->load->library("memcached_library");
		$mensajes_pizarra = $this->memcached_library->get('pizarra_torneo_'.$torneo_id);
		if (!$mensajes_pizarra)
		{
			$this->load->model("torneo","torneo",true);
			$mensajes_pizarra = $this->torneo->listarMensajes($torneo_id);
			if (!$mensajes_pizarra)
				$mensajes_pizarra = "vacio";
			$this->memcached_library->add('pizarra_torneo_'.$torneo_id,$mensajes_pizarra,1800);
		}

		return $mensajes_pizarra;
	}

	public function _leer_datos_palpitos()
	{
		$this->load->library('memcached_library');
		$partidos_palpitos24 = $this->memcached_library->get('palpitos24');
		if (!$partidos_palpitos24)
		{
			$url_canal='palpitos24.com.ar/bannerLG/api.php';
			$handler = curl_init();  
			curl_setopt($handler, CURLOPT_URL, $url_canal);

			curl_setopt($handler, CURLOPT_RETURNTRANSFER, true); 
			curl_setopt($handler, CURLOPT_PROXY, PROXY_HOST);
			curl_setopt($handler, CURLOPT_PROXYPORT, PROXY_PORT);
			$response = curl_exec ($handler);
			$httpCode = curl_getinfo($handler, CURLINFO_HTTP_CODE);
			curl_close($handler);
			
			$doc = new SimpleXmlElement($response);
			$cantidad =count($doc->Partido);
			for ($i=0; $i < $cantidad; $i++) 
			{ 
				$palpito[$i]['descripcion'] = (string)$doc->Partido[$i]->attributes()->Descripcion;
				$palpito[$i]['id'] = (string)$doc->Partido[$i]->attributes()->ID;
				$palpito[$i]['modalidad'] = (string)$doc->Partido[$i]->Oferta->attributes()->Modalidad;
				$palpito[$i]['local'] = (string)$doc->Partido[$i]->Oferta[0];
				$palpito[$i]['empate'] = (string)$doc->Partido[$i]->Oferta[1];
				$palpito[$i]['visita'] = (string)$doc->Partido[$i]->Oferta[2];	
			}
			$partidos_palpitos24 = $palpito;
			$this->memcached_library->add('palpitos24',$partidos_palpitos24,3600);
		}

		return $partidos_palpitos24;
		//return false;
	}
	public function _verificar_participacion()
	{
		$this->load->library("memcached_library");
		$user = $this->lg_api->get_data();
		if($user)
		{
			$this->load->model('prode','prode',true);
			//Chequeo si ya hizo palpitos
			$tiene_palpitos = $this->memcached_library->get('tiene_palpitos_'.$user['id']);
			if(!$tiene_palpitos)
			{
				$tiene_palpitos=$this->prode->tienePalpitos($user['id']);
				$this->memcached_library->add('tiene_palpitos_'.$user['id'], $tiene_palpitos, 1200);
			}
			
			if(!$tiene_palpitos)
			{
				$info_usuario = $this->memcached_library->get('usuario_info_'.$user['id']);
				if(!$info_usuario)
				{
					$info_usuario=$this->prode->dameInfoUsuario($user['id']);
					$this->memcached_library->add('usuario_info_'.$user['id'], $info_usuario, 1200);
				}
			}

			$variables['tiene_palpitos'] = $tiene_palpitos;
			$variables['usuario_info'] = $info_usuario;
			$variables['desde'] = 1;

			return $variables;
		}
		else
			return false;
	}
}
?>