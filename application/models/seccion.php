<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Seccion extends CI_Model {

	public function getListado()
	{
		$query = $this->db->get('seccion');
		$resultado = $query->result_array();
		if($resultado>0)
		{
			return $resultado;
		}
		else
			return 0;
	}

	public function save($id,$datos)
	{
		if ($id)
		{
			$this->db->where('id',$id);
			if ($this->db->update('seccion',$datos))
				return true;
			else
				return false;
		}
		else
		{
			if($this->db->insert('seccion',$datos))
				return true;
			else
				return false;		
		}
	}

	public function delete($id)
	{
		if ($this->db->delete('seccion',array('id' => $id) ) )
			return true;
		else
			return false;
	}

}

/* End of file seccion.php */
/* Location: ./application/models/seccion.php */