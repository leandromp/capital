<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Departamento extends CI_Model{

  public function __construct()
  {
    parent::__construct();
    //Codeigniter : Write Less Do More
  }

  public function getListado()
  {
    $sql="SELECT d.*,n.descripcion as nivel FROM departamento d  LEFT JOIN nivel n on (d.nivel_id=n.id_nivel)";
    $query=$this->db->query($sql);
    $resultado=$query->result_array();
    if(count($resultado)>0)
      return $resultado;
    else
      return false;
  }

  public function save($id,$datos)
  {
    if ($id)
    {
      $this->db->where("id",$id);
      if($this->db->update('departamento',$datos))
        return true;
      else
        return false;
    }
    else
    {
      if($this->db->insert('departamento',$datos))
        return true;
      else
        return false;
        echo 'hola';
    }
  }

  public function getDepartamentoById($id)
  {
    $sql="SELECT d.*,n.descripcion as nivel, n.id_nivel as nivel_id FROM departamento d  LEFT JOIN nivel n on (d.nivel_id=n.id_nivel) 
    WHERE d.id=".$id;
    $query=$this->db->query($sql);
    
    $resultado=$query->result_array();
    if(count($resultado)>0)
      return $resultado[0];
    else
      return false;
  }

  public function delete($id)
  {
    if ($this->db->delete('departamento',array('id' => $id) ) )
      return true;
    else
      return false;
  }

  public function getCantidadPuestoByDepartamentoId($id)
  {
    $sql="select count(id) as resultado from puesto where id_departamento=".$id;

    $query=$this->db->query($sql);
    
    $resultado=$query->result_array();
    if(count($resultado)>0)
      return $resultado[0]['resultado'];
    else
      return false;
  }

  public function getPuestoByDepartamentoId()
  {
    $sql="select d.descripcion,p.nombre from departamento d  
          inner join puesto p on (d.id=p.id_departamento)
          group by p.nombre";

    $query=$this->db->query($sql);
    
    $resultado=$query->result_array();
    if(count($resultado)>0)
      return $resultado;
    else
      return false;
  }

   public function getPuestoByDepartamento()
  {
    $sql="SELECT d.descripcion,p.nombre from departamento d  
          inner join puesto p on (d.id=p.id_departamento)
          group by p.nombre ORDER BY d.descripcion ";

    $query=$this->db->query($sql);
    
    $resultado=$query->result_array();
    if(count($resultado)>0)
      return $resultado;
    else
      return false;
  }

   public function getCantidadEmpleadosByDepartamentoId($id)
  {
    $sql="SELECT e.nombre,p.nombre as puesto,d.descripcion as departamento from empleado e
          inner join historico h on (e.id=h.empleado_id)
          inner join puesto p on (h.puesto_id = p.id)
          inner join departamento d on (d.id = p.id_departamento)
          where d.id=".$id."
          group by e.nombre";

    $query=$this->db->query($sql);
    
    $resultado=$query->result_array();
    if(count($resultado)>0)
      return count($resultado);
    else
      return false;
  }

  public function getDepartamentosByNivel()
  {
    $sql ="SELECT d.descripcion,n.descripcion as nivel from departamento d
            inner join nivel n on (d.nivel_id = n.id_nivel)
            order by nivel";
     $query=$this->db->query($sql);
    
    $resultado=$query->result_array();
    if(count($resultado)>0)
      return $resultado;
    else
      return false;
  }

  public function getResponsableNivel()
  {
    $sql ="SELECT e.nombre, p.nombre as puesto, d.descripcion as departamento from historico h
            inner join puesto p on (h.puesto_id = p.id and p.responsable=1)
            inner join empleado e on (h.empleado_id = e.id)
            inner join departamento d on (p.id_departamento = d.id)";
     $query=$this->db->query($sql);
    
    $resultado=$query->result_array();
    if(count($resultado)>0)
      return $resultado;
    else
      return false;
  }



  public function getEmpleadosActuales()
  {
    $sql="SELECT * FROM (SELECT count(empleado_id) as cantidad,empleado_id from historico where actual=1
          group by empleado_id) as tabla where cantidad>1 ";
    $query = $this->db->query($sql);
    $resultado=$query->result_array();
    if(count($resultado)>0)
      return $resultado;
    else
     return false;
  }



  public function getEmpleadosMasPuestos($id_empleado)
  {
    $sql="SELECT e.id,e.nombre as empleado,e.legajo, p.nombre as puesto ,p.descripcion, j.descripcion as jerarquia, d.descripcion as departamento, h.actual,n.descripcion as nivel from empleado e left join historico h on (e.id=h.empleado_id) left join puesto p on (h.puesto_id = p.id) left join jerarquia j on (p.id_jerarquia = j.id_jerarquia) left join departamento d on (p.id_departamento = d.id) left join nivel n on (d.nivel_id = n.id_nivel) where h.actual=1 and e.id=".$id_empleado;
    $query = $this->db->query($sql);
    $resultado=$query->result_array();
    if(count($resultado)>0)
      return $resultado;
    else
     return false;
  }

  public function getEmpleadosByDeptoId($id_departamento)
  {
    $sql="SELECT e.nombre as empleado,h.fecha, d.descripcion as departamento,n.descripcion as nivel
    , p.nombre as puesto, h.actual,p.id,j.descripcion as jerarquia FROM
      empleado e
    INNER JOIN historico h on (e.id=h.empleado_id)
    inner join puesto p on (h.puesto_id=p.id)
    inner join departamento d on (p.id_departamento = d.id)
    left join jerarquia j on (p.id_jerarquia = j.id_jerarquia)
      INNER JOIN nivel n on (d.nivel_id=n.id_nivel) where h.actual=1 and d.id=".$id_departamento;
    $query = $this->db->query($sql);
    $resultado=$query->result_array();
    if(count($resultado)>0)
      return $resultado;
    else
     return false;
  }


}
