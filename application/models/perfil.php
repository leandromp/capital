<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	
class Perfil extends CI_Model {
		
	public function getPerfiles($perfil_codigo=0)
	{	
		$sql = "SELECT id,nombre,codigo FROM perfil where codigo >= ".$perfil_codigo;
		$query=$this->db->query($sql);
		$resultado=$query->result_array();
		if(count($resultado)>0)
			return $resultado;
		else
			return false;
	}

	public function insert($datos)
	{
		if($this->db->insert('perfil',$datos))
			return true;
		else
			return false;
	}

	public function getPerfilId($id)
	{
		$query=$this->db->get_where("perfil",array("id"=>$id));
		$resultado=$query->result_array();
			return $resultado[0];
	}

	public function update($id,$datos)
	{	
		$this->db->where("id",$id);
		if($this->db->update("perfil",$datos))
			return true;
		else
			return false;
	}

	public function delete($id)
	{
		if ($this->db->delete('perfil',array('id' => $id) ) )
			return true;
		else
			return false;
	}


}

	/* End of file perfil.php */
	/* Location: ./application/models/perfil.php */
?>