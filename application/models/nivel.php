<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Nivel extends CI_Model{

  public function __construct()
  {
    parent::__construct();
    //Codeigniter : Write Less Do More
  }

  public function getListado()
  {
    $query=$this->db->get("nivel");
    $resultado=$query->result_array();
    if(count($resultado)>0)
      return $resultado;
    else
      return false;
  }

  public function save($id,$datos)
  {
    if ($id)
    {
      $this->db->where("id_nivel",$id);
      if($this->db->update("nivel",$datos))
        return true;
      else
        return false;
    }
    else
    {
      if($this->db->insert('nivel',$datos))
        return true;
      else
        return false;
        echo 'hola';
    }
  }

  public function getNivelById($id)
  {

    $query=$this->db->get_where('nivel',array('id_nivel'=>$id));
    $resultado=$query->result_array();
    if(count($resultado)>0)
      return $resultado[0];
    else
      return false;
  }

  public function delete($id)
  {
    if ($this->db->delete('nivel',array('id_nivel' => $id) ) )
      return true;
    else
      return false;
  }

}
