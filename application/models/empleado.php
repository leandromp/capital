<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Empleado extends CI_Model {

	public function getEmpleados($perfil_id=0)
	{
		if($perfil_id!=0)
			$this->db->where("perfil_id",$perfil_id);

		$query=$this->db->get("empleado");
		$resultado=$query->result_array();
		if(count($resultado)>0)
			return $resultado;
		else
			return false;
	}

	public function getListado()
	{
		$sql="select * from empleado";
		$query=$this->db->query($sql);
		$resultado=$query->result_array();
		if(count($resultado)>0)
			return $resultado;
		else
			return false;
	}



	public function insert($datos)
	{
		if($this->db->insert('usuario',$datos))
			return true;
		else
			return false;
	}

	public function getEmpleadoId($id)
	{
		$query=$this->db->get_where("empleado",array("id"=>$id));
		$resultado=$query->result_array();
			return $resultado[0];
	}

	public function save($id,$datos)
	{
		if ($id)
		{
			$this->db->where("id",$id);
			if($this->db->update("empleado",$datos))
				return true;
			else
				return false;
		}
		else
		{
			if($this->db->insert('empleado',$datos))
				return true;
			else
				return false;
		}
	}

	public function delete($id)
	{
		if ($this->db->delete('empleado',array('id' => $id) ) )
			return true;
		else
			return false;
	}

	public function getPuestos()
	{
		$sql = "SELECT p.id, CONCAT(p.nombre,'-',UPPER(d.descripcion))	 as puesto from puesto p
				 		left join departamento d on (p.id_departamento = d.id)";
		$query=$this->db->query($sql);
		if ($query->num_rows() > 0)
		{
			$resultado = $query->result_array();
			return $resultado;
		}
		else
			return false;
	}

	public function noAsignado($empleado_id,$puesto_id)
	{
		$sql="select count(id) as cantidad from historico where empleado_id=".$empleado_id." AND puesto_id=".$puesto_id;
		
		$query=$this->db->query($sql);
		$resultado=$query->result_array();
		
		if ($resultado[0]['cantidad'] == 0)
		{
			return 1;
		}
		else
			return 2;
	}

	public function addPuesto($datos)
	{
		if($this->db->insert('historico',$datos))
			echo 'ok';
		else
			echo 'ko';
	}


	public function getInstancias($empleado_id)
	{
		$sql="SELECT e.nombre,h.fecha, d.descripcion as departamento,n.descripcion as nivel
		, p.nombre as puesto, h.actual FROM
			empleado e
		INNER JOIN historico h on (e.id=h.empleado_id)
		inner join puesto p on (h.puesto_id=p.id)
		inner join departamento d on (p.id_departamento = d.id)
        INNER JOIN nivel n on (d.nivel_id=n.id_nivel) where e.id=".$empleado_id;
		$query=$this->db->query($sql);
		if ($query->num_rows() > 0)
		{
			$resultado = $query->result_array();
			return $resultado;
		}
		else
			return false;
	}

	
	public function getHistoricoUnico($empleado_id)
	{
		$sql="SELECT e.nombre,h.fecha, d.descripcion as departamento,n.descripcion as nivel, TIMESTAMPDIFF(YEAR,h.fecha,CURDATE()) as anio, (TIMESTAMPDIFF(MONTH,h.fecha,CURDATE()) - TIMESTAMPDIFF(YEAR,h.fecha,CURDATE())*12) as meses,
		(TIMESTAMPDIFF(DAY,h.fecha,CURDATE()) - (TIMESTAMPDIFF(YEAR,h.fecha,CURDATE())*365)) as dias

		, p.nombre as puesto FROM
			empleado e
		INNER JOIN historico h on (e.id=h.empleado_id)
		inner join puesto p on (h.puesto_id=p.id)
		inner join departamento d on (p.id_departamento = d.id)
        INNER JOIN nivel n on (d.nivel_id=n.id_nivel) where e.id=".$empleado_id;
		$query=$this->db->query($sql);
		if ($query->num_rows() > 0)
		{
			$resultado = $query->result_array();
			return $resultado;
		}
		else
			return false;
	}

	public function getHistoricoByFechas($fecha_inicio,$fecha_fin,$empleado_id)
	{

		$sql="SELECT TIMESTAMPDIFF(YEAR,'".$fecha_inicio."','".$fecha_fin."') as anio, (TIMESTAMPDIFF(MONTH,'".$fecha_inicio."','".$fecha_fin."') - TIMESTAMPDIFF(YEAR,'".$fecha_inicio."','".$fecha_fin."')*12) as meses,
		(TIMESTAMPDIFF(DAY,'".$fecha_inicio."','".$fecha_fin."') - (TIMESTAMPDIFF(YEAR,'".$fecha_inicio."','".$fecha_fin."')*365)) as dias ";    
		$query=$this->db->query($sql);
		if ($query->num_rows() > 0)
		{
			$resultado = $query->result_array();
			return $resultado;
		}
		else
			return false;
	}

}
?>
