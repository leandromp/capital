<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Jerarquia extends CI_Model{

  public function __construct()
  {
    parent::__construct();
    //Codeigniter : Write Less Do More
  }

  public function getListado()
  {
    $query=$this->db->get("jerarquia");
    $resultado=$query->result_array();
    if(count($resultado)>0)
      return $resultado;
    else
      return false;
  }

  public function save($id,$datos)
  {
    if ($id)
    {
      $this->db->where("id_jerarquia",$id);
      if($this->db->update("jerarquia",$datos))
        return true;
      else
        return false;
    }
    else
    {
      if($this->db->insert('jerarquia',$datos))
        return true;
      else
        return false;
        echo 'hola';
    }
  }

  public function getJerarquiaById($id)
  {

    $query=$this->db->get_where('jerarquia',array('id_jerarquia'=>$id));
    $resultado=$query->result_array();
    if(count($resultado)>0)
      return $resultado[0];
    else
      return false;
  }

  public function delete($id)
  {
    if ($this->db->delete('jerarquia',array('id_jerarquia' => $id) ) )
      return true;
    else
      return false;
  }

}
