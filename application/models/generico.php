<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Generico extends CI_Model {

	public function update($campo,$valor,$id,$tabla)
	{
		$sql= " UPDATE ".$tabla." SET ".$campo."='".$valor."'";

		if($tabla=='producto_cantidad')
			$sql.=" WHERE producto_codigo=".$id;
		else
			$sql.=" WHERE id=".$id;

		if($this->db->query($sql))
			return true;
		else
			return false;

	}

	public function getValueId($id,$campo,$tabla)
	{
		$sql = "SELECT ".$campo." as resultado FROM ".$tabla." WHERE id=".$id;
		$consulta = $this->db->query($sql);
		$resultado = $consulta->result_array();
		if($resultado>0)
			return $resultado[0]['resultado'];
		else
			return false;
	}


}

/* End of file generico.php */
/* Location: ./application/models/generico.php */