<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	
class Usuario extends CI_Model {
		
	public function getUsuarios($perfil_id=0)
	{	
		if($perfil_id!=0)
			$this->db->where("perfil_id",$perfil_id);
		
		$query=$this->db->get("usuario");
		$resultado=$query->result_array();
		if(count($resultado)>0)
			return $resultado;
		else
			return false;
	}

	public function getListado()
	{
		$sql="select u.id, CONCAT(u.nombre,' ',u.apellido) as nombre, u.email,u.ultimo_acceso,
				if(u.habilitado=1, 'SI','NO') as habilitado
				, p.nombre as perfil_nombre
				from usuario u 
				inner join perfil p on (u.perfil_id=p.id)";
		$query=$this->db->query($sql);
		$resultado=$query->result_array();
		if(count($resultado)>0)
			return $resultado;
		else
			return false;
	}

	public function comprobarLogeo($correo,$password)
	{
		$sql = "SELECT u.id, u.nombre, u.apellido, u.email, u.habilitado, u.perfil_id, 
		date_format(u.ultimo_acceso,'%d/%m/%Y %H:%i') as ultimo_acceso, p.nombre as perfil, p.codigo 
				FROM usuario u
				INNER JOIN perfil p on (u.perfil_id=p.id)
				WHERE email='".$correo."' AND password=MD5('".$password."')";
		$query=$this->db->query($sql);
		if ($query->num_rows() > 0)
		{
			$resultado = $query->result_array();
			return $resultado[0];
		}
		else
			return false;
	}
	
	public function getPermisos($perfil_id,$modulo)
	{
		$sql="select p.alta,p.baja,p.mod,p.listado from
				permiso p 
				where p.accion = '".$modulo."' and p.perfil_id=".$perfil_id;
		$query=$this->db->query($sql);
		$resultado=$query->result_array($query);
		if(count($resultado)>0)
			return $resultado[0];
		else
			return false;
	}

	public function getPerfiles()
	{
		$query=$this->db->get("perfil");
		$resultado=$query->result_array();
		if(count($resultado)>0)
			return $resultado;
		else
			return false;

	}

	public function insert($datos)
	{
		if($this->db->insert('usuario',$datos))
			return true;
		else
			return false;
	}

	public function getUsuarioId($id)
	{
		$query=$this->db->get_where("usuario",array("id"=>$id));
		$resultado=$query->result_array();
			return $resultado[0];
	}

	public function getUsuarioEmail($email)
	{
		$query=$this->db->get_where("usuario",array("email"=>$email));
		$resultado=$query->result_array();
			return $resultado[0];
	}

	public function save($id,$datos)
	{	
		if ($id)
		{
			$this->db->where("id",$id);
			if($this->db->update("usuario",$datos))
				return true;
			else
				return false;
		}
		else
		{
			if($this->db->insert('usuario',$datos))
				return true;
			else
				return false;
		}
	}

	public function delete($id)
	{
		if ($this->db->delete('usuario',array('id' => $id) ) )
			return true;
		else
			return false;
	}

	public function validaEmail($email, $id)
	{
		$sql = "select id from usuario where email = '".$email."'";
		if ($id > 0)
			$sql .= " and id <> ".$id;
		$query=$this->db->query($sql);
		if ($query->num_rows() == 0)
			return true;
		else
			return false;
	}
}
?>