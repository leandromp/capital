<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Puesto extends CI_Model {

	public function getListado()
  {
    $sql="SELECT p.*,j.descripcion as jerarquia from puesto p 
left join jerarquia j on (p.id_jerarquia=j.id_jerarquia)";
    $query=$this->db->query($sql);
    $resultado=$query->result_array();
    if(count($resultado)>0)
      return $resultado;
    else
      return false;
  }

  public function save($id,$datos)
  {
    if ($id)
    {
      $this->db->where("id",$id);
      if($this->db->update("puesto",$datos))
        return true;
      else
        return false;
    }
    else
    {
      if($this->db->insert('puesto',$datos))
        return true;
      else
        return false;
        echo 'hola';
    }
  }

  public function getPuestoById($id)
  {

    $sql="SELECT p.*,d.descripcion as departamento from puesto p inner join departamento d on (p.id_departamento=d.id) where p.id=".$id;
    $query=$this->db->query($sql);
    $resultado=$query->result_array();
    if(count($resultado)>0)
      return $resultado[0];
    else
      return false;
  }

  public function delete($id)
  {
    if ($this->db->delete('puesto',array('id' => $id) ) )
      return true;
    else
      return false;
  }

}

/* End of file puesto.php */
/* Location: ./application/views/modulos/puesto.php */
