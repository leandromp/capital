<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Menu_modelo extends CI_Model {

	public function dameMenu($perfil_id=1)
	{
		$sql = "SELECT m.* FROM permiso p
				INNER JOIN modulo m on (p.modulo_id = m.id)
				WHERE p.perfil_id=".$perfil_id." ORDER BY orden";
		$query=$this->db->query($sql);
		$resultado = $query->result_array();
		return $resultado;
	}

	public function getAllMenu()
	{
		$query = $this->db->get('menu');
		$resultado = $query->result_array();
		if (count($resultado)>0)
		{
			return $resultado;
		}
		else
			return false;
	}

	public function getAll($id,$padre_id=0)
	{
		$sql = "select me.id, me.titulo,me.enlace_externo as enlace ,me.orden, p.titulo as pagina_nombre
				from menu_elemento me
				left join pagina p on (me.objeto_id_enlace = p.id)
				WHERE menu_id=".$id;
		if($id>0)
			$sql.=" and  me.padre_id=".$padre_id;
		else
			$sql.=" and me.padre_id<=0 ";

		$sql.= " ORDER BY me.orden";
		$query = $this->db->query($sql);
		$resultado = $query->result_array();
		if (count($resultado)>0)
		{
			return $resultado;
		}
		else
			return false;
	}

	public function save($id,$datos)
	{
		if ($id)
		{
			$this->db->where('id',$id);
			if ($this->db->update('menu_elemento',$datos))
				return true;
			else
				return false;
		}
		else
		{
			if($this->db->insert('menu_elemento',$datos))
				return true;
			else
				return false;
		}
	}

	public function delete($id)
	{
		if ($this->db->delete('menu_elemento',array('id' => $id) ) )
			return true;
		else
			return false;
	}

	public function save_menu($id,$datos)
	{
		if ($id)
		{
			$this->db->where('id',$id);
			if ($this->db->update('menu',$datos))
				return true;
			else
				return false;
		}
		else
		{
			if($this->db->insert('menu',$datos))
				return true;
			else
				return false;
		}
	}

	public function delete_menu($id)
	{
		if ($this->db->delete('menu_elemento',array('id' => $id) ) )
			return true;
		else
			return false;
	}

	public function getInfoElementoId($menu_id)
	{
		if($query = $this->db->get_where('menu_elemento',array('id'=>$menu_id)))
		{
			$resultado = $query->result_array();
			return $resultado;
		}
		else
			return false;

	}

}

	/* End of file menu.php */
	/* Location: ./application/models/menu.php */
?>
