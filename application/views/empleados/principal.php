<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
       <?= $nombre=strtoupper($modulo_nombre=$this->uri->segment(1));?>
    </h1>
</section>
<!-- Main content -->
<section class="content">
    <div class="md-col-1">
        <a href="<?=site_url($modulo_nombre.'/alta')?>">
            <button type="button" class="btn btn-success btn-flat"> <span class="fa fa-plus"> </span> Nuevo <?=substr($this->uri->segment(1),0,-1)?> </button>
        </a>
    </div>
    <br>
    <div class="box">
        <div class="box-header" id="recargar">
            <div class="col-md-11"> <h3 class="box-title">Listado de <?=$nombre?></h3> </div>
        </div><!-- /.box-header -->
        <div class="box-body">
            <table id="tabla-busqueda" class="table table-bordered table-striped">
                <thead>
                    <tr>
                        <th>ID Usuario</th>
                        <th>Nombre</th>
                        <th>Legajo</th>
                        <th>Habilitado</th>
                        <th data-sortable="false">Acciones</th>
                    </tr>
                </thead>
                <tbody>
                    <?foreach ($empleados as $key => $u) :?>
                        <tr id="fila_<?=$u['id']?>">
                            <td><?=$u['id']?></td>
                            <td><?=$u['nombre']?></td>
                            <td><?=$u['legajo']?></td>
                             <td><?=$u['habilitado']?></td>

                            <td>
                              <a href="<?=site_url($modulo_nombre.'/puesto/'.$u['id']);?>" class="btn  btn-default" title="Puesto"> <i class="fa fa-building"></i> </a> &nbsp;&nbsp;
                                <a href="<?=site_url($modulo_nombre.'/editar/'.$u['id']);?>" class="btn  btn-default" title="Editar"> <i class="fa fa-edit"></i> </a> &nbsp;&nbsp;
                            <a class="btn  btn-danger" href="javascript:void(0)" onclick="eliminar(<?=$u['id']?>,'<?=$modulo_nombre?>')" title="Eliminar"> <i class="fa fa-trash-o"></i> </a> </td>
                        </tr>
                    <?endforeach;?>
                </tbody>
            </table>
        </div><!-- /.box-body -->
    </div><!-- /.box -->
</section>
