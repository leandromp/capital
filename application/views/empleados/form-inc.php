<? $accion=$this->uri->segment(2); ?>
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
       <?= strtoupper($modulo_nombre=$this->uri->segment(1));?>
    </h1>
</section>
<!-- Main content -->
<section class="content">
    <div class="box box-primary">
    <form id="empleados-form" role="form" method="post" action="<?=site_url('empleados/guardar')?>">
       <div class="box-header">
       <h3 class="box-title"><?=strtoupper($accion)?></h3>
       </div>
       <div class="box-body">
          <div class="form-group">
                <input id="id" readonly class="form-control" type="hidden" name="id" placeholder="id del usuario" value="<? if(isset($user)) echo $user['id'];?>" >
          </div>
           <div class="form-group" id="nombre">
                <label for="exampleInputEmail1">Nombre</label>
                <input class="form-control require" type="text" name="nombre" placeholder="Nombre" value="<? if(isset($user)) echo $user['nombre'];?>" >
            </div>

            <div class="form-group" id="dni">
                <label for="exampleInputEmail1">DNI</label>
                <input class="form-control require" type="text" name="dni" placeholder="dni" value="<? if(isset($user)) echo $user['dni'];?>">
            </div>

            <div class="form-group" id="legajo">
                <label for="exampleInputEmail1">Legajo</label>
                <input class="form-control require" type="text" name="legajo" placeholder="legajo" value="<? if(isset($user)) echo $user['legajo'];?>">
            </div>

            <div class="form-group" id="perfil_id">
                <label for="exampleInputEmail1">Perfil</label>
                <select name="id_usuario" class="form-control require">
                    <option value="0" >Seleccione un usuario</option>
                  <? foreach($usuarios as $p): ?>
                    <option  value="<?=$p['id']?>" <? if($user['id_usuario'] == $p['id']) echo 'selected'; ?>> <?=$p['nombre']?> </option>
                  <? endforeach; ?>
                </select>      
            </div>

            <!--<div class="form-group" id="password">
                <label for="exampleInputEmail1">Password</label>
                <input class="form-control" type="password" name="password" placeholder="password" value="">
            </div>-->

            <? if ($permiso):?>
            <!--<div class="form-group" id="perfil_id">
                <label for="exampleInputEmail1">Perfil</label>
                <select name="perfil_id" class="form-control require">
                    <option value="0" >Seleccione un perfil</option>
                  <? foreach($perfiles as $p): ?>
                    <option  value="<?=$p['id']?>" <? if($user['perfil_id'] == $p['id']) echo 'selected'; ?>> <?=$p['nombre']?> </option>
                  <? endforeach; ?>
                </select>
            </div>-->
            <? endif;?>

            <? if ($permiso):?>
            <div class="form-group">
                <label for="exampleInputEmail1">Habilitado</label>
                <? if(isset($user) and $user['habilitado']==1)
                    $chek = "checked";
                  else
                    $chek = "";
                ?>

                <input id="habilitado" <?=$chek?> value="1" class="checkbox" type="checkbox" name="habilitado" placeholder="habilitado" >
            </div>
            <? else:?>
              <input type="hidden" value="<?=$user['habilitado']?>" name="habilitado">
            <? endif;?>
            <div class="box-footer">
              <? if ($permiso):?>
              <button class="btn btn-primary" type="button" id="btnguardar" onclick="guardarGeneral('empleados-form','empleados')">Guardar</button>
              <img src="<?=site_url('img/loading.gif')?>" id="loading" style="display:none;">
              <a href="<?=site_url('empleados')?>" class="btn btn-danger">Volver</a>
              <? else:?>
              <button class="btn btn-primary" type="button" id="btnguardar" onclick="guardarGeneral('empleados-form','')">Guardar</button>
              <img src="<?=site_url('img/loading.gif')?>" id="loading" style="display:none;">
              <? endif;?>
            </div>
       </div>

    </form>
    </div>
</section>
