<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
      ADMINISTRACIÓN DE PUESTOS
    </h1>
</section>
<!-- Main content -->
<section class="content">
    <div class="md-col-1">

            <button data-toggle="modal" data-target="#modalPuesto" type="button" class="btn btn-success btn-flat"> <span class="fa fa-plus"> </span> Agregar puesto </button>

    </div>
    <br>
    <div class="box">
        <div class="box-header" id="recargar">
            <div class="col-md-11"> <h3 class="box-title">Historial de puesto</h3> </div>
        </div><!-- /.box-header -->
        <div class="box-body">
            <table id="tabla-busqueda" class="table table-bordered table-striped">
                <thead>
                    <tr>
                        <th>Nombre</th>
                        <th>Puesto</th>
                        <th>Departamento</th>
                        <th>Nivel</th>
                        <th>Antiguedad</th>
                        <th>Actual</th>
  
                    </tr>
                </thead>
                <tbody>
                    <?foreach ($historico as $key => $u) :?>
                        <tr id="fila_<?=$u['id']?>">

                            <td><?=$u['nombre']?></td>
                            <td><?=$u['puesto']?></td>
                            <td><?=$u['departamento']?></td>
                            <td><?=$u['nivel']?></td>
                            <td><?=$u['anio'].'años - '.$u['meses'].' meses - '.$u['dias'].'dias' ?></td>
                            <td><? echo ($u['actual']==1)? 'si': 'no'; ?></td>
                        </tr>
                    <?endforeach;?>
                </tbody>
            </table>
        </div><!-- /.box-body -->
    </div><!-- /.box -->
</section>
<!-- Modal -->
<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="modalPuesto" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="modal_titulo">Agregar puesto</h4>
            </div>
            <div class="modal-body">

                <form id="foto-nota" method="post" enctype="multipart/form-data">
                <div id="error" style="visibility: hidden;" class="alert alert-danger alert-dismissible"> </div>

                    <div class="form-group" id="puesto">
                          <label for="exampleInputEmail1">Departamento</label>
                          <select id="puesto" name="puesto" class="form-control">
                              <option value="0" >Seleccione un departamento</option>
                            <? foreach($puestos as $d): ?>
                            <option  value="<?=$d['id']?>"> <?=$d['puesto']?> </option>
                            <? endforeach; ?>
                          </select>
                      </div>

                  <div class="form-group col-md-12">
                  <label for="exampleInputEmail1">Fecha</label>
                  <input id="fecha" class="form-control" type="date" name="fecha" placeholder="Orden">
                  </div>
                  <div class="form-group col-md-12">
                    <label for="exampleInputEmail1">Actual</label>
                     <input id="actual" <?=$chek?> value="1" class="checkbox" type="checkbox" name="actual" placeholder="actual" >
                  </div>
                  <div class="form-group col-md-12">
                  <input id="empleado_id" class="form-control" type="hidden" name="empleado_id" value="<?=$empleado_id?>">
                  </div>

                </form>
            </div>
            <div class="modal-footer">
              <p class="text-red" id="message" style="display:none;"></p>
              <div id="modal_botones col-md-12">
              <button onclick="agregar_puesto()" class="btn btn-default" type="button" id="btn-cancel-fp" data-loading-text="Cancelar">Enviar</button>
              </div>
            </div>
        </div>
    </div>
</div>
<!-- modal -->
