<!DOCTYPE html>
<html>
  <?php $this->load->view('head'); ?>
  <body class="login-page">
  <div class="login-box">
    <div class="login-logo">
      <a href="<?=site_url('')?>"><b>Capital </b>Humano</a>
    </div>
    <div class="login-box-body">
      <p class="login-box-msg">Iniciá tu Sesión</p>

      <div class="form-group has-feedback">
        <input type="text" class="form-control" name="correo" placeholder="E-mail" id="email"/>
        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
      </div>
      <div class="form-group has-feedback">
        <input type="password" class="form-control" name="password" placeholder="Password" id="pass"/>
        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
      </div>
      <p class="text-red" id="error" style="display:none;"></p>
      <div class="row">
        <div class="col-xs-6">
          <a data-toggle="modal" href="#myModal">Olvide mi contrase&ntilde;a</a>
        </div>
        <div class="col-xs-6">
          <button type="button" id="btnlogin" class="btn btn-primary btn-block btn-flat" onclick="login()">Iniciar Sesion</button>
          <img src="<?=site_url('img/loading.gif')?>" id="loading" style="display:none;" class="btn">
        </div>
      </div>

      <!-- Modal -->
      <div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="myModal" class="modal fade">
          <div class="modal-dialog">
              <div class="modal-content">
                  <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                      <h4 class="modal-title">¿Olvidates tu contraseña?</h4>
                  </div>
                  <div class="modal-body">
                      <p>Ingresá tu dirección de correo para cambiar tu contraseña.</p>
                      <input type="text" name="email_fp" id="email_fp" placeholder="E-mail" autocomplete="off" class="form-control placeholder-no-fix">

                  </div>
                  <div class="modal-footer">
                    <p class="text-red" id="message" style="display:none;"></p>
                    <button data-dismiss="modal" class="btn btn-default" type="button" id="btn-cancel-fp" data-loading-text="Cancelar">Cancelar</button>
                    <button class="btn btn-primary" type="button" id="btn-enviar-fp" data-loading-text="Enviando..." onclick="enviar_password()">Enviar</button>
                  </div>
              </div>
          </div>
      </div>
      <!-- modal -->

    </div>
  </div>
  <script>var URL_BASE='<?=site_url();?>';</script>
  <script src="<?=site_url('js/jquery.min.js')?>"></script>
  <script src="<?=site_url('js/bootstrap/bootstrap.min.js')?>" type="text/javascript"></script>
  <script src="<?=site_url('js/login.js')?>"></script>
</body>
