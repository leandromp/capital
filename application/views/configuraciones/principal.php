<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
       <?=strtoupper($nombre_pagina);?>
    </h1>
</section>
<section class="content">
    <div class="md-col-1">
       <button data-toggle="modal" type="button" data-target="#modalConfiguracion" class="btn btn-success btn-flat"> <span class="fa fa-plus"> </span> Nuevo Valor   </button> 
    </div>
    <br>
    <div class="box">
        <div class="box-header" id="recargar">
            <div class="col-md-11"> <h3 class="box-title">Listado de <?=$nombre?></h3> </div>
        </div><!-- /.box-header -->
        <div class="box-body">
            <table id="tabla-busqueda" class="table table-bordered table-striped">
                <thead>
                    <tr>
                        <th>Tipo </th>
                        <th>Codigo</th>
                        <th>Descripcion</th>
                        <th data-sortable="false">Acciones</th>
                    </tr>
                </thead>
                <tbody>
                    <?foreach ($droguerias as $key => $d) :?>
                        <tr id="fila_<?=$d['item']?>">
                            <td><? if($d['codigo']==0) echo 'LABORATORIO'; ?> </td>
                            <td><?=$d['item']?></td>
                            <td><?=$d['descripcion']?></td>             
                            <td>
                            <a class="btn btn-danger" href="javascript:void(0)" onclick="eliminar_configuracion(<?=$d['item']?>,<?=$d['codigo']?>)" title="Eliminar"> <i class="fa fa-trash-o"></i> </a> </td>
                        </tr>
                    <?endforeach;?>
                </tbody>
            </table>
        </div><!-- /.box-body -->
    </div><!-- /.box -->
</section>
<!-- Modal -->
<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="modalConfiguracion" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="modal_titulo">Agregar Dorgueria</h4>
            </div>
            <div class="modal-body"> 
                
                <form id="seccion" method="post" action="<?=site_url('pagos/guardar-pagos')?>" enctype="multipart/form-data"> 
                <p>
                <div class="form-group">
                <label for="exampleInputEmail1">Nombre</label>
                <input id="descripcion" class="form-control require col-md-4" type="text" name="descripcion" placeholder="descripcion">
                </div>
                <div class="form-group">
                <label>Tipo</label>
                <select id="tipo" class="form-control require">
                    <option value="0">Laboratorio</option>
                    
                </select>
               </div>
                </p>
                <div style="clear:both;"> </div>
                </form>
            </div>
            <div class="modal-footer">
              <p class="text-red" id="message" style="display:none;"></p>
              <div id="modal_botones col-md-12">
              <button onclick="agregarConfiguracion();" class="btn btn-default" type="button" id="btn-cancel-fp" data-loading-text="Cancelar">Agregar</button>
              </div>
            </div>
        </div>
    </div>
</div>