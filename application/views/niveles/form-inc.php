<? $accion=$this->uri->segment(2); ?>
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
       <?= strtoupper($modulo_nombre=$this->uri->segment(1));?>
    </h1>
</section>
<!-- Main content -->
<section class="content">
    <div class="box box-primary">
    <form id="usuarios-form" role="form" method="post" action="<?=site_url('niveles/guardar')?>">
       <div class="box-header">
       <h3 class="box-title"><?=strtoupper($accion)?></h3>
       </div>
       <div class="box-body">
          <div class="form-group">
                <input id="id" readonly class="form-control" type="hidden" name="id" placeholder="id del usuario" value="<? if(isset($nivel)) echo $nivel['id_nivel'];?>" >
          </div>
           <div class="form-group" id="descripcion">
                <label for="exampleInputEmail1">Descripcion</label>
                <input class="form-control require" type="text" name="descripcion" placeholder="Descripción" value="<? if(isset($nivel)) echo $nivel['descripcion'];?>" >
            </div>


            <div class="box-footer">
              <? if ($permiso):?>
              <button class="btn btn-primary" type="button" id="btnguardar" onclick="guardarGeneral('usuarios-form','niveles')">Guardar</button>
              <img src="<?=site_url('img/loading.gif')?>" id="loading" style="display:none;">
              <a href="<?=site_url('niveles')?>" class="btn btn-danger">Volver</a>
              <? else:?>
              <button class="btn btn-primary" type="button" id="btnguardar" onclick="guardarGeneral('usuarios-form','')">Guardar</button>
              <img src="<?=site_url('img/loading.gif')?>" id="loading" style="display:none;">
              <? endif;?>
            </div>
       </div>

    </form>
    </div>
</section>
