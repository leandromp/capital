<header class="main-header">
  <a href="<?=site_url('')?>" class="logo">
    <!-- mini logo for sidebar mini 50x50 pixels -->
    <span class="logo-mini"><b>A</b>D10</span>
    <!-- logo for regular state and mobile devices -->
    <span class="logo-lg"><b>Admin</b></span>
  </a>
  <nav class="navbar navbar-static-top" role="navigation">
    <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
      <span class="sr-only">Toggle navigation</span>
    </a>
    <div class="navbar-custom-menu">
      <ul class="nav navbar-nav">
        <li class="dropdown user user-menu">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            <img src="<?=site_url('img/avatar.jpg')?>" class="user-image" alt="User Image"/>
            <span class="hidden-xs"><?=$usuario['nombre'].' '.$usuario['apelido']?></span>
          </a>
          <ul class="dropdown-menu">
            <li class="user-header">
              <img src="<?=site_url('img/avatar.jpg')?>" class="img-circle" alt="User Image" />
              <p><?=$usuario['nombre'].' '.$usuario['apellido']?><small><?=$usuario['ultimo_acceso']?></small></p>
            </li>
            <li class="user-footer">
              <div class="pull-left">
                <a href="<?=site_url('usuarios/editar/'.$usuario['id'])?>" class="btn btn-default btn-flat">Perfil</a>
              </div>
              <div class="pull-right">
                <a href="<?=site_url('usuarios/cerrar_sesion')?>" class="btn btn-default btn-flat">Cerrar Sesion</a>
              </div>
            </li>
          </ul>
        </li>
      </ul>
    </div>
  </nav> 
</header>