<!DOCTYPE html>
<html>
  <? $this->load->view('head'); ?>
  <body class="skin-blue sidebar-mini">
    <div class="wrapper">

        <?$this->load->view('header')?>
        <?$this->load->view('menu-principal'); ?>

        <div class="content-wrapper">
            <section class="content-header"></section>
            <!-- Main content -->
            <? $this->load->view('dashboard/dashboard'); ?>
            <section class="content">
                <?=$error;?>
            </section>
        </div>

        <? $this->load->view("pie")?>
    </div>
</body>
</html>