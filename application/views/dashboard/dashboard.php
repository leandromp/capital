<div class="col-md-12">
<div class="col-md-6">
<h2> Reportes </h2>
	<a target="_blank" href="<?=URL_BASE.'pdfs/responsable_nivel'?>" style="margin-bottom: 5px;" class="btn col-md-12 btn-lg btn-primary"> <span class="fa fa-bar-chart"> </span> Responsables por departamento</a><br>

  	<a target="_blank" href="<?=URL_BASE.'pdfs/cantidad_departamentos'?>" style="margin-bottom: 5px;" class="btn col-md-12 btn-lg btn-success"> <span class="fa fa-bar-chart"> </span>  Dpto. Empresa </a><br>

    <a target="_blank" href="<?=URL_BASE.'pdfs/cantidad_puestos_departamentos'?>" style="margin-bottom: 5px;" class="btn col-md-12 btn-lg btn-primary"> <span class="fa fa-bar-chart"> </span>  Cantidad de puestos por departamento  </a><br>

     <a target="_blank" href="<?=URL_BASE.'pdfs/puestos_departamentos'?>" style="margin-bottom: 5px;" class="btn col-md-12 btn-lg btn-success">
     <span class="fa fa-bar-chart"> </span> Puestos por departamento  </a><br>

      <a target="_blank" href="<?=URL_BASE.'pdfs/cantidad_empleados_departamentos'?>" style="margin-bottom: 5px;" class="btn col-md-12 btn-lg btn-primary"> <span class="fa fa-bar-chart"> </span> Empleados por departamento  </a><br>

      <a target="_blank" href="<?=URL_BASE.'pdfs/departamentos_nivel'?>" style="margin-bottom: 5px;" class="btn col-md-12 btn-lg btn-success"> <span class="fa fa-bar-chart"> </span> Departamentos x nivel</a><br>


      <a target="_blank" href="<?=URL_BASE.'pdfs/puestos_departamentos_bis'?>" style="margin-bottom: 5px;" class="btn col-md-12 btn-lg btn-primary"> <span class="fa fa-bar-chart"> </span> Departamentos x puesto</a><br>

      <a target="_blank" href="<?=URL_BASE.'pdfs/cantidad_empleados_departamentos'?>" style="margin-bottom: 5px;" class="btn col-md-12 btn-lg btn-success"> <span class="fa fa-bar-chart"> </span>  Cantidad de empleados por departamento  </a><br>

      <a target="_blank" href="<?=URL_BASE.'pdfs/empleados_dos_puestos'?>" style="margin-bottom: 5px;" class="btn col-md-12 btn-lg btn-primary"> <span class="fa fa-bar-chart"> </span> Empleados con mas de un puesto  </a><br>

      
          <div class="form-group">
                          <label for="exampleInputEmail1">Departamento</label>
                          <select id="id_departamento" name="puesto" class="form-control">
                              <option value="0" >Seleccione un departamento</option>
                            <? foreach($departamentos as $d): ?>
                            <option  value="<?=$d['id']?>"> <?=$d['descripcion']?> </option>
                            <? endforeach; ?>
                          </select>
                      </div>
      <button onclick="mostrarEmpleadosByDepartamento()" style="margin-bottom: 5px;" class="btn col-md-12 btn-lg btn-primary"> <span class="fa fa-bar-chart"> </span> Mostrar empleados  </button><br>
      
</div>
</div>