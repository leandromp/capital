<aside class="main-sidebar">
  <section class="sidebar">
    <div class="user-panel">
      <div class="pull-left image">
        <img src="<?=site_url('img/avatar.jpg')?>" class="img-circle" alt="User Image" />
      </div>
      <div class="pull-left info">
        <p><?=$usuario['nombre'].' '.$usuario['apellido']?></p>
        <a href="<?=site_url('')?>"><i class="fa fa-circle text-success"></i> Online</a>
      </div>
    </div>
    <ul class="sidebar-menu">
      <li class="header">Men&uacute;</li>
      <? foreach ($menu as $key => $m): ?>
        <li>
          <a href="<?=site_url($m['accion']);?>">
            <i class="fa <?=$m['icono']?>"></i>
            <span><?= $m['titulo'] ?></span> 
           </a>
        </li>
      <? endforeach; ?>
    </ul>
  </section>
</aside>