
 <!--   <aside class="right-side">-->
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
               <?= strtoupper($modulo_nombre=$this->uri->segment(1));?>
            </h1>
            <!--<ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                <li><a href="#">Examples</a></li>
                <li class="active">Blank page</li>
            </ol>-->
        </section>
        <!-- Main content -->
        <section class="content">
            <div class="md-col-1">
                <a href="javascript:void(0)" onclick="agregar_permisos()"> <button type="button" class="btn btn-success btn-flat"> Agregar Modulo </button> </a> 

            </div>
            <br>
             <form role="form" method="post" action="<?=site_url('modulos/permisos/1');?>">
            <div class="box">
                        <div class="box-header" id="recargar">
                            <div class="col-md-11"> <h3 class="box-title">Permisos para el perfil</h3>
                                
                             </div>
                             <div class="col-md-6 form-group">
                                <select name="perfil_id" id="perfil" onchange="cambiar_perfil();">
                                    <option value="0">Seleccione un Perfil</option>
                                    <? foreach ($perfiles as $k => $v):?>
                                    <option <?if($perfil_seleccionado==$v['id']) echo 'selected'?> value="<?=$v['id']?>"> <?=$v['nombre']?> </option>
                                    <?endforeach;?>
                                </select>
                                </div>
                        </div><!-- /.box-header -->
                        <div id="cuerpo"> 
                        <? if(isset($perfil_seleccionado) and $perfil_seleccionado>0): ?>
                          <? $this->load->view('modulos/listado-permisos-inc'); ?>
                        <? endif; ?>
                       <?/* </div><!-- cierra el div cuerpo contenedor -->
                        <div class="box-footer">
                        <button class="btn btn-primary"  type="submit">Guardar Cambios</button> 
                        <a href="<?=site_url($modulo_nombre)?>"> <button class="btn btn-danger" type="button"> Volver </button> </a>
                        </div> */?>
                        </form>
                        </div><!-- /.box-body -->
                    </div><!-- /.box -->
                    <? if(isset($mensaje)): ?>
                        <div class="alert alert-info alert-dismissable"> <?=$mensaje?> </div>
                    <? endif ?>
                    <?if(isset($error)): ?>
                        <div class="alert alert-danger alert-dismissable"> <?=$error?> </div>
                    <?endif?>
        </section>
    <!--</aside>--> <!-- /.right-side -->
 <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="
        true">&times;</span><span class="sr-only">Cerrar</span></button>

        <h4 class="modal-title" id="myModalLabel">Agregar Modulo</h4>
      </div>
      <div class="modal-body">
      
            <div id="modulos" class="form-group">
            
            </div>
            
            
            <br>
          </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
        <button id="btn_agregar_modulo"onclick="agregar_modulo_perfil();" type="button" class="btn btn-primary">Guardar Cambios</button>
      </div>
    </div>
  </div>
</div>