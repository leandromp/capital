<? $accion=$this->uri->segment(2); ?>
<!-- Content Header (Page header) -->

<section class="content-header">
    <h1>
       <?= strtoupper($modulo_nombre=$this->uri->segment(1));?>
    </h1>
</section>
<!-- Main content -->
<section class="content">
    <div class="box box-primary">
    <form id="modulos-form" role="form" method="post" action="<?=site_url($modulo_nombre.'/'.$accion)?>">
       <div class="box-header">
       <h3 class="box-title"><?=strtoupper($accion)?></h3> 
       </div>
       <div class="box-body">
          <div class="form-group">
                <input id="id" readonly class="form-control" type="hidden" name="id" placeholder="id del modulo" value="<? if(isset($modulo)) echo $modulo['id'];?>" >
          </div> 
           <div class="form-group">
                <label for="exampleInputEmail1">Nombre</label>
                <input id="titulo" class="form-control" type="text" name="titulo" placeholder="titulo del modulo" value="<? if(isset($modulo)) echo $modulo['titulo'];?>" >
            </div>

            <div class="form-group">
                <label for="exampleInputEmail1">Accion</label>
                <input id="accion" class="form-control" type="text" name="accion" placeholder="Accion" value="<? if(isset($modulo)) echo $modulo['accion'];?>">
            </div>

            <div class="form-group">
                <label for="exampleInputEmail1">Icono</label>
                <input id="icono" class="form-control" type="text" name="icono" placeholder="Icono" value="<? if(isset($modulo)) echo $modulo['icono'];?>">
            </div>

            <div class="form-group">
                <label for="exampleInputEmail1">Orden</label>
                <input id="orden" class="form-control" type="text" name="orden" placeholder="orden" value="<? if(isset($modulo)) echo $modulo['orden'];?>">
            </div>

                <input id="hijos" <?=$chek?> class="checkbox" type="checkbox" name="hijos" placeholder="hijos" >
            </div>
            <div class="box-footer">
            <button class="btn btn-primary"  type="submit">Guardar</button> 
            <a href="<?=site_url($modulo_nombre)?>"> <button class="btn btn-danger" type="button"> Volver </button> </a>
            </div>
       </div>

    </form>
    </div>

</section>
