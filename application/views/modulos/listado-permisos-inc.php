<div class="box-body table-responsive">
    <table id="example1" class="table table-bordered table-hover">
        <thead>
            <tr>
                <th>Nombre</th>
                <th>Alta</th>
                <th>Baja</th>
                <th>Modificacion</th>
                <th>Consulta / Listado</th>
            </tr>
        </thead>
        <tbody>
            <?foreach ($permisos as $key => $value) :?>
                <tr>
                    <td><?=$value['titulo']?></td>
                    <td><input name="<?=$value['modulo_id']?>-1" type="checkbox" <? if($value['alta']==1) echo 'checked'?> > </td>
                    <td><input name="<?=$value['modulo_id']?>-2" type="checkbox" <? if($value['baja']==1) echo 'checked'?> >  </td>
                    <td><input name="<?=$value['modulo_id']?>-3" type="checkbox" <? if($value['mod']==1) echo 'checked'?> >  </td>
                    <td><input name="<?=$value['modulo_id']?>-4" type="checkbox" <? if($value['listado']==1) echo 'checked'?> >  </td>
                </tr>    
            <?endforeach;?>
        </tbody>
        
        <tfoot>

        </tfoot>
    </table>
    </div><!-- cierra el div cuerpo contenedor -->
    <div class="box-footer">
    <button class="btn btn-primary"  type="submit">Guardar Cambios</button> 
    <a href="<?=site_url('modulos')?>"> <button class="btn btn-danger" type="button"> Volver </button> </a>
    </div>