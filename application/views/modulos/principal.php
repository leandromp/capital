<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
       <?= $nombre=strtoupper($modulo_nombre=$this->uri->segment(1));?>
    </h1>
</section>
<!-- Main content -->
<section class="content">
    <div class="md-col-1">
        <a href="<?=site_url('modulos/alta/1')?>"> 
            <button type="button" class="btn btn-success btn-flat"> <span class="fa fa-plus"> </span> Nuevo <?=$this->uri->segment(1)?> </button> 
        </a> 
        <a href="<?=site_url('modulos/permisos')?>"> <button type="button" class="btn btn-success btn-flat"> <span class="fa fa-lock"> </span> Permisos</button> </a> 
        <a href="<?=site_url('modulos/limpiar_db')?>"> <button type="button" class="btn btn-danger btn-flat"> <span class="fa fa-lock"> </span> Limpiar DB</button> </a> 
    </div>
    <br>
    <div class="box">
        <div class="box-header" id="recargar">
            <div class="col-md-11"> <h3 class="box-title">Listado de <?=$nombre?></h3> </div>
        </div><!-- /.box-header -->
        <div class="box-body">
            <table id="tabla-busqueda" class="table table-bordered table-striped">
                <thead>
                    <tr>
                        <th>ID Modulo</th>
                        <th>Titulo</th>
                        <th>Accion</th>
                        <th>Icono</th>
                        <th>Hijos</th>
                        <th>Orden</th>
                        <th data-sortable="false">Acciones</th>
                    </tr>
                </thead>
                <tbody>
                    <?foreach ($menu as $key => $value) :?>
                        <tr id="fila_<?=$value['id']?>">
                            <td><?=$value['id']?></td>
                            <td><?=$value['titulo']?></td>
                            <td><?=$value['accion']?></td>
                            <td><?=$value['icono']?></td>
                            <td><?=$value['hijos']?></td>
                            <td><?=$value['orden']?></td>
                            <td><a href="<?=site_url($modulo_nombre.'/editar/'.$value['id']);?>" class="btn btn-default" title="Editar"> <i class="fa fa-edit"></i> </a> &nbsp;&nbsp;
                            <a href="javascript:void(0)" class="btn btn-danger" onclick="eliminar(<?=$value['id']?>,'<?=$modulo_nombre?>')" title="Eliminar"> <i class="fa fa-trash-o"></i> </td>
                        </tr>
                    <?endforeach;?>
                </tbody>
            </table>
        </div><!-- /.box-body -->
    </div><!-- /.box -->
</section>

