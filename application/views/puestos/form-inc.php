<? $accion=$this->uri->segment(2); ?>
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
       <?= strtoupper($modulo_nombre=$this->uri->segment(1));?>
    </h1>
</section>
<!-- Main content -->
<section class="content">
    <div class="box box-primary">
    <form id="usuarios-form" role="form" method="post" action="<?=site_url('puestos/guardar')?>">
       <div class="box-header">
       <h3 class="box-title"><?=strtoupper($accion)?></h3>
       </div>
       <div class="box-body">
          <div class="form-group">
                <input id="id" readonly class="form-control" type="hidden" name="id" placeholder="id del usuario" value="<? if(isset($puesto)) echo $puesto['id'];?>" >
          </div>
               <div class="form-group" id="nombre">
                <label for="exampleInputEmail1">Nombre</label>
                <input class="form-control require" type="text" name="nombre" placeholder="Descripción" value="<? if(isset($puesto)) echo $puesto['nombre'];?>" >
            </div>
           <div class="form-group" id="descripcion">
                <label for="exampleInputEmail1">Descripcion</label>
                <textarea class="form-control require" type="text" name="descripcion" placeholder="Descripción" > <? if(isset($puesto)) echo $puesto['descripcion'];?>  </textarea>
            </div>
            <div class="form-group">
                <label for="exampleInputEmail1">Responsable</label> 
                <? if(isset($puesto) and $puesto['responsable']==1)
                    $chek = "checked";
                  else
                    $chek = "";
                ?>

                <input id="responsable" <?=$chek?> class="checkbox" type="checkbox" name="responsable" placeholder="responsable" >
            </div>
            <div class="form-group" id="departamento_id">
                  <label for="exampleInputEmail1">Departamento</label>
                  <select name="departamento_id" class="form-control">
                      <option value="0" >Seleccione un departamento</option>
                    <? foreach($departamentos as $d): ?>
                    <option  value="<?=$d['id']?>" <? if($puesto['id_departamento'] == $d['id']) echo 'selected'; ?>> <?=$d['descripcion']?> </option>
                    <? endforeach; ?>
                  </select>
              </div>

              <div class="form-group" id="id_jerarquia">
                  <label for="exampleInputEmail1">Jerarquia</label>
                  <select name="id_jerarquia" class="form-control">
                      <option value="0" >Seleccione una jerarquia</option>
                    <? foreach($jerarquias as $j): ?>
                    <option  value="<?=$j['id_jerarquia']?>" <? if($puesto['id_jerarquia'] == $j['id_jerarquia']) echo 'selected'; ?>> <?=$j['descripcion']?> </option>
                    <? endforeach; ?>
                  </select>
              </div>

            <div class="box-footer">
              <? if ($permiso):?>
              <button class="btn btn-primary" type="button" id="btnguardar" onclick="guardarGeneral('usuarios-form','puestos')">Guardar</button>
              <img src="<?=site_url('img/loading.gif')?>" id="loading" style="display:none;">
              <a href="<?=site_url('puestos')?>" class="btn btn-danger">Volver</a>
              <? else:?>
              <button class="btn btn-primary" type="button" id="btnguardar" onclick="guardarGeneral('usuarios-form','')">Guardar</button>
              <img src="<?=site_url('img/loading.gif')?>" id="loading" style="display:none;">
              <? endif;?>
            </div>
       </div>

    </form>
    </div>
</section>
