<? $accion=$this->uri->segment(2); ?>
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
       <?= strtoupper($modulo_nombre=$this->uri->segment(1));?>
    </h1>
</section>
<!-- Main content -->
<section class="content">
    <div class="box box-primary">
    <form id="departamentos-form" role="form" method="post" action="<?=site_url('departamentos/guardar')?>">
       <div class="box-header">
       <h3 class="box-title"><?=strtoupper($accion)?></h3>
       </div>
       <div class="box-body">
          <div class="form-group">
                <input id="id" readonly class="form-control" type="hidden" name="id" placeholder="id del usuario" value="<? if(isset($departamento)) echo $departamento['id'];?>" >
          </div>
           <div class="form-group" id="descripcion">
                <label for="exampleInputEmail1">Nombre</label>
                <input class="form-control require" type="text" name="descripcion" placeholder="Descripción" value="<? if(isset($departamento)) echo $departamento['descripcion'];?>" >
            </div>
             <div class="form-group col-md-6" id="fecha_alta">
                <label for="exampleInputEmail1">Fecha alta</label>
                <input class="form-control require" type="date" name="fecha_alta" placeholder="fecha" value="<? if(isset($departamento)) echo $departamento['fecha_alta'];?>" >
            </div>
            <div class="col-md-6">
            <div class="form-group" id="nivel_id">
                  <label for="exampleInputEmail1">Nivel</label>
                  <select name="nivel_id" class="form-control">
                      <option value="0" >Seleccione un Nivel</option>
                    <? foreach($niveles as $l): ?>
                    <option  value="<?=$l['id_nivel']?>" <? if($departamento['nivel_id'] == $l['id_nivel']) echo 'selected'; ?>> <?=$l['descripcion']?> </option>
                    <? endforeach; ?>
                  </select>      
              </div>
            </div>
          </div>
            <div class="box-footer">
              <? if ($permiso):?>
              <button class="btn btn-primary" type="button" id="btnguardar" onclick="guardarGeneral('departamentos-form','departamentos')">Guardar</button>
              <button data-toggle="modal" data-target="#modalPuesto" type="button" class="btn btn-success btn-flat"> <span class="fa fa-plus"> </span> Agregar nivel </button>
              <img src="<?=site_url('img/loading.gif')?>" id="loading" style="display:none;">
              <a href="<?=site_url('departamentos')?>" class="btn btn-danger">Volver</a>
              <? else:?>
              <button class="btn btn-primary" type="button" id="btnguardar" onclick="guardarGeneral('departamentos-form','')">Guardar</button>
              <img src="<?=site_url('img/loading.gif')?>" id="loading" style="display:none;">
              <? endif;?>
            </div>
       </div>

    </form>
    </div>
</section
<!-- Modal -->
<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="modalPuesto" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="modal_titulo">Agregar Nivel</h4>
            </div>
            <div class="modal-body">

                <form id="foto-nota" method="post" enctype="multipart/form-data">
                     <div class="form-group col-md-12">
                        <label for="exampleInputEmail1">Nombre</label>
                        <input id="nivel_nombre" class="form-control" type="text" name="nivel_nombre" placeholder="nivel" value="" >
                    </div>

                     <div class="form-group">
                <input id="id" readonly class="form-control" type="hidden" name="id" placeholder="id del usuario" value="<? if(isset($departamento)) echo $departamento['id'];?>" >
          </div>

                </form>
            </div>
            <div class="modal-footer">
              <p class="text-red" id="message" style="display:none;"></p>
              <div id="modal_botones col-md-12">
              <button onclick="agregar_nivel()" class="btn btn-default" type="button" id="btn-cancel-fp" data-loading-text="Cancelar">Enviar</button>
              </div>
            </div>
        </div>
    </div>
</div>
<!-- modal -->