<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
       <?= $nombre=strtoupper($modulo_nombre=$this->uri->segment(1));?>
    </h1>
</section>
<!-- Main content -->
<section class="content">
    <div class="md-col-1">
        <a href="<?=site_url($modulo_nombre.'/alta')?>">
            <button type="button" class="btn btn-success btn-flat"> <span class="fa fa-plus"> </span> Nuevo Departamento </button>
        </a>
    </div>
    <br>
    <div class="box">
        <div class="box-header" id="recargar">
            <div class="col-md-11"> <h3 class="box-title">Listado de <?=$nombre?></h3> </div>
        </div><!-- /.box-header -->
        <div class="box-body">
            <table id="tabla-busqueda" class="table table-bordered table-striped">
                <thead>
                    <tr>
                        <th>ID Departamento</th>
                        <th>Nombre</th>
                        <th>Descripcion Nivel</th>
                        <th data-sortable="false">Acciones</th>
                    </tr>
                </thead>
                <tbody>
                    <?foreach ($departamentos as $key => $d) :?>
                        <tr id="fila_<?=$d['id']?>">
                            <td><?=$d['id']?></td>
                            <td><?=$d['descripcion']?></td>
                            <td><?=$d['nivel']?></td>
                            <td>
                                <a href="<?=site_url($modulo_nombre.'/editar/'.$d['id']);?>" class="btn btn-table btn-default" title="Editar"> <i class="fa fa-edit"></i> </a> &nbsp;&nbsp;
                            <a class="btn btn-table btn-danger" href="javascript:void(0)" onclick="eliminar(<?=$d['id']?>,'<?=$modulo_nombre?>')" title="Eliminar"> <i class="fa fa-trash-o"></i> </a> </td>
                        </tr>
                    <?endforeach;?>
                </tbody>
            </table>
        </div><!-- /.box-body -->
    </div><!-- /.box -->
</section>
