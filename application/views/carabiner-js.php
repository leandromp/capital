    <script>
        var URL_BASE='<?=site_url();?>';
        var controlador_base = '<?=$this->uri->segment(1)?>';
    </script>
    <!-- jQuery 2.1.4 -->
    <script src="<?=site_url()?>js/jquery.min.js"></script>
    <!-- jQuery UI 1.11.2 -->
    <script src="<?=site_url()?>js/jquery-ui.min.js"></script>
    <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
    <script>
      $.widget.bridge('uibutton', $.ui.button);
    </script>
    <!-- Bootstrap 3.3.2 JS -->
    <script src="<?=site_url()?>js/bootstrap/bootstrap.min.js" type="text/javascript"></script>
    <!-- Boostrap Validator -->
    <script src="<?=site_url()?>js/bootstrap/bootstrapValidator.min.js" type="text/javascript"></script>
    <!-- Morris.js charts -->
    <script src="http://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
    <script src="<?=site_url()?>js/plugins/morris/morris.min.js" type="text/javascript"></script>
    <!-- Sparkline -->
    <script src="<?=site_url()?>js/plugins/sparkline/jquery.sparkline.min.js" type="text/javascript"></script>
    <!-- jvectormap -->
    <script src="<?=site_url()?>js/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js" type="text/javascript"></script>
    <script src="<?=site_url()?>js/plugins/jvectormap/jquery-jvectormap-world-mill-en.js" type="text/javascript"></script>
    <!-- jQuery Knob Chart -->
    <script src="<?=site_url()?>js/plugins/knob/jquery.knob.js" type="text/javascript"></script>
    <!-- daterangepicker -->
    <!--<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.2/moment.min.js" type="text/javascript"></script>-->
    <!--<script src="<?=site_url()?>js/plugins/daterangepicker/daterangepicker.js" type="text/javascript"></script>-->
    <!-- datepicker -->
    <script src="<?=site_url()?>js/plugins/datepicker/bootstrap-datepicker.js" type="text/javascript"></script>
    <!-- Bootstrap WYSIHTML5 -->
    <script src="<?=site_url()?>js/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js" type="text/javascript"></script>
    <!-- Slimscroll -->
    <script src="<?=site_url()?>js/plugins/slimScroll/jquery.slimscroll.min.js" type="text/javascript"></script>
    <!-- FastClick -->
    <!--<script src='<?=site_url()?>js/plugins/fastclick/fastclick.min.js'></script>-->
    <!-- AdminLTE App -->
    <script src="<?=site_url()?>js/dist/js/app.min.js" type="text/javascript"></script>
    <!-- DATA TABES SCRIPT -->
    <script src="<?=site_url()?>js/plugins/datatables/jquery.dataTables.min.js" type="text/javascript"></script>
    <script src="<?=site_url()?>js/plugins/datatables/dataTables.bootstrap.min.js" type="text/javascript"></script>
    <!-- iCheck -->
    <script src="<?=site_url()?>js/plugins/iCheck/icheck.min.js" type="text/javascript"></script>
    <!-- estos 2 son exclusivos del proyecto -->
    <script src="<?=site_url()?>js/app.js" type="text/javascript"></script>
    <script src="<?=site_url()?>js/validacion.js" type="text/javascript"></script>
    <!-- CKEditor -->
    <script src="<?=site_url()?>js/plugins/ckeditor/ckeditor.js" type="text/javascript"></script>
    <!-- EasyUI -->
    <script src="<?=site_url()?>js/plugins/easyui.js" type="text/javascript"></script>
    <!-- Drag and Drop menu -->
    <!--<script src="<?=site_url()?>js/plugins/nestable/nestable.js" type="text/javascript"></script> -->
     <!-- Drag and Drop menu -->
    <script src="<?=site_url()?>js/plugins/select2/select2.full.min.js" type="text/javascript"></script>
