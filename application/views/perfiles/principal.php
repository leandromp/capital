<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
       <?= $nombre=strtoupper($modulo_nombre=$this->uri->segment(1));?>
    </h1>
</section>
<!-- Main content -->
<section class="content">
    <div class="md-col-1">
        <a href="<?=site_url('perfiles/alta/1')?>"> 
            <button type="button" class="btn btn-success btn-flat"> <span class="fa fa-plus"> </span> Nuevo Perfil </button> 
        </a> 
        <!--<a href="<?=site_url('perfiles/permisos')?>"> <button type="button" class="btn btn-success btn-flat"> <span class="fa fa-lock"> </span> Permisos</button> </a> -->
    </div>
    <br>
    <div class="box">
        <div class="box-header" id="recargar">
            <div class="col-md-11"> <h3 class="box-title">Listado de <?=$nombre?></h3> </div>
        </div><!-- /.box-header -->
        <div class="box-body">
            <table id="tabla-busqueda" class="table table-bordered table-striped">
                <thead>
                    <tr>
                        <th>ID Modulo</th>
                        <th>Titulo</th>
                        <th>Codigo</th>
                        <th data-sortable="false">Acciones</th>
                    </tr>
                </thead>
                <tbody>
                    <?foreach ($perfiles as $key => $per) :?>
                        <tr id="fila_<?=$per['id']?>">
                            <td><?=$per['id']?></td>
                            <td><?=$per['nombre']?></td>
                            <td><?=$per['codigo']?></td>
                         
                            <td><a href="<?=site_url($modulo_nombre.'/editar/'.$per['id']);?>" class="btn btn-table btn-default" title="Editar"> <i class="fa fa-edit"></i> </a> &nbsp;&nbsp;
                            <a href="javascript:void(0)" class="btn btn-table btn-danger" onclick="eliminar(<?=$per['id']?>,'<?=$modulo_nombre?>')" title="Eliminar"> <i class="fa fa-trash-o"></i> </td>
                        </tr>
                    <?endforeach;?>
                </tbody>
            </table>
        </div><!-- /.box-body -->
    </div><!-- /.box -->
     <?if(isset($error) and $error!=""):?>
             <div class="alert alert-danger alert-dismissable">
                <i class="fa fa-ban"></i>
                      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                              <b>Alerta!</b> <?=$error;?>
            </div>
        <?endif;?>
        <?if(isset($mensaje) and $mensaje!=""):?>
         <div class="alert alert-info alert-dismissable">
            <i class="fa fa-ban"></i>
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                          <b>Alerta!</b> <?=$mensaje;?>
        </div>
        <?endif;?>
</section>