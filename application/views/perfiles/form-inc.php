<? $accion=$this->uri->segment(2); ?>
<!-- Content Header (Page header) -->

<section class="content-header">
    <h1>
       <?= strtoupper($modulo_nombre=$this->uri->segment(1));?>
    </h1>
</section>
<!-- Main content -->
<section class="content">

    <div class="box box-primary">
    <form id="perfiles-form" role="form" method="post" action="<?=site_url($modulo_nombre.'/'.$accion)?>">
       <div class="box-header">
       <h3 class="box-title"><?=strtoupper($accion)?></h3> 
       </div>
       <div class="box-body">
          <div class="form-group">
                <input id="id" readonly class="form-control" type="hidden" name="id" placeholder="id del perfil" value="<? if(isset($perfil)) echo $perfil['id'];?>" >
          </div> 
           <div class="form-group">
                <label for="exampleInputEmail1">Nombre</label>
                <input id="nombre" class="form-control" type="text" name="nombre" placeholder="Codigo del perfil" value="<? if(isset($perfil)) echo $perfil['nombre'];?>" >
            </div>

            <div class="form-group">
                <label for="exampleInputEmail1">Codigo</label>
                <input id="codigo" class="form-control" type="text" name="codigo" placeholder="Codigo" value="<? if(isset($perfil)) echo $perfil['codigo'];?>">
            </div>

            
           
            <div class="box-footer">
            <button class="btn btn-primary"  type="submit">Guardar</button> 
            <a href="<?=site_url($modulo_nombre)?>"> <button class="btn btn-danger" type="button"> Volver </button> </a>
            </div>
       </div>

    </form>
    </div>

</section>
