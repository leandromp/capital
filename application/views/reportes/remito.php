<link href="<?=site_url('css/tcpdf.css')?>" rel="stylesheet" type="text/css" />
<table class="tabla">
	<tr class="seccion">
		<td> Remito N <?=$venta['id'] ?> </td>
		<td colspan="2"> Fecha <?=$this->varios_library->transformar_fecha($venta['fecha']);?></td>
	</tr>
	<tr class="datos-cliente">
		<td> Cliente : <?=$venta['rsocial'] ?> </td>
		<td> Direcci&oacute;n : <?=$venta['direccion'] ?> </td>
		<td> Tel&eacute;fono: <?=$venta['telefono']?></td>
	</tr>
	<tr class="datos-cliente">
		<td> CUIL/CUIT : <?=$venta['cuit'] ?></td>
		<td> email: <?=$venta['email'] ?></td>
		<td> cufe : <?=$venta['cufe'] ?></td>
	</tr>
</table>

<table class="detalle-venta"> 
<tr>
	<th> Cantidad </th>
	<th> Descripcion </th>
	<th> Laboratorio </th>
	<th> Vencimiento </th>
	<th> Precio Unitario </th>
	<th> Subtotal </th>
</tr>
	<? foreach ($venta['detalle'] as $det) : ?>
		<tr> 
			<td> <?=$det['cantidad']?> </td>
			<td> <?=$det['descripcion']?> </td>
			<td> <?=$det['laboratorio']?> </td>
			<td> <?=$det['vencimiento']?> </td>
			<td> <?=$det['precio']?> </td>
			<td> <?=$det['precio']*$det['cantidad']?> </td>
		</tr> 
	<? endforeach; ?>
<tr>
	<td class="total-venta" colspan="5"> Total:<?=$venta['total']?> </td><td>ORIGINAL</td>
</tr>
</table>
<br> --------------------------------------------------------------------------------------------------<br>
<table class="tabla">
	<tr class="seccion">
		<td> Remito N <?=$venta['id'] ?> </td>
		<td colspan="2"> Fecha <?=$this->varios_library->transformar_fecha($venta['fecha']);?></td>
	</tr>
	<tr class="datos-cliente">
		<td> Cliente : <?=$venta['rsocial'] ?> </td>
		<td> Direcci&oacute;n : <?=$venta['direccion'] ?> </td>
		<td> Tel&eacute;fono: <?=$venta['telefono']?></td>
	</tr>
	<tr class="datos-cliente">
		<td> CUIL/CUIT : <?=$venta['cuit'] ?></td>
		<td> email: <?=$venta['email'] ?></td>
		<td> cufe : <?=$venta['cufe'] ?></td>
	</tr>
</table>

<table class="detalle-venta"> 
<tr>
	<th> Cantidad </th>
	<th> Descripcion </th>
	<th> Laboratorio </th>
	<th> Vencimiento </th>
	<th> Precio Unitario </th>
	<th> Subtotal </th>
</tr>
	<? foreach ($venta['detalle'] as $det) : ?>
		<tr> 
			<td> <?=$det['cantidad']?> </td>
			<td> <?=$det['descripcion']?> </td>
			<td> <?=$det['laboratorio']?> </td>
			<td> <?=$det['vencimiento']?> </td>
			<td> <?=$det['precio']?> </td>
			<td> <?=$det['precio']*$det['cantidad']?> </td>
		</tr> 
	<? endforeach; ?>
<tr>
	<td class="total-venta" colspan="5"> Total:<?=$venta['total']?> </td><td>COPIA</td>
</tr>
</table>