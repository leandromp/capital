<!-- Bootstrap 3.3.4 -->
<link href="<?=site_url('css/bootstrap/bootstrap.min.css')?>" rel="stylesheet" type="text/css" />    
<!-- FontAwesome 4.3.0 -->
<link href="<?=site_url('css/font-awesome/font-awesome.min.css')?>" rel="stylesheet" type="text/css" />
<!-- Ionicons 2.0.0 -->
<!--<link href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css" rel="stylesheet" type="text/css" />    -->
<!-- Theme style -->
<link href="<?=site_url('css/dist/AdminLTE.min.css')?>" rel="stylesheet" type="text/css" />
<!-- AdminLTE Skins. Choose a skin from the css/skins 
     folder instead of downloading all of them to reduce the load. -->
<link href="<?=site_url('css/dist/skins/_all-skins.min.css')?>" rel="stylesheet" type="text/css" />
<!-- iCheck -->
<link href="<?=site_url('css/plugins/iCheck/flat/blue.css')?>" rel="stylesheet" type="text/css" />
<!-- Morris chart -->
<!--<link href="<?=site_url('css/plugins/morris/morris.css')?>" rel="stylesheet" type="text/css" />-->
<!-- jvectormap -->
<!--<link href="<?=site_url('css/plugins/jvectormap/jquery-jvectormap-1.2.2.css')?>" rel="stylesheet" type="text/css" />-->
<!-- Date Picker -->
<link href="<?=site_url('css/plugins/datepicker/datepicker3.css')?>" rel="stylesheet" type="text/css" />
<!-- Daterange picker -->
<link href="<?=site_url('css/plugins/daterangepicker/daterangepicker-bs3.css')?>" rel="stylesheet" type="text/css" />
<!-- bootstrap wysihtml5 - text editor -->
<link href="<?=site_url('css/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css')?>" rel="stylesheet" type="text/css" />
<!-- Data tables -->
<link href="<?=site_url('css/plugins/datatables/dataTables.bootstrap.css')?>" rel="stylesheet" type="text/css" />
<!-- Validator Jquery Css -->
<link href="<?=site_url('css/bootstrap/bootstrapValidator.min.css');?>" rel="stylesheet" type="text/css" />
<!-- Data tables -->
<link href="<?=site_url('css/plugins/select2/select2.min.css')?>" rel="stylesheet" type="text/css" />