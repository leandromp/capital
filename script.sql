-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Versión del servidor:         5.7.11-0ubuntu6 - (Ubuntu)
-- SO del servidor:              Linux
-- HeidiSQL Versión:             9.2.0.4947
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Volcando estructura de base de datos para capital
CREATE DATABASE IF NOT EXISTS `capital` /*!40100 DEFAULT CHARACTER SET utf8 COLLATE utf8_bin */;
USE `capital`;


-- Volcando estructura para tabla capital.configuracion
CREATE TABLE IF NOT EXISTS `configuracion` (
  `codigo` int(11) DEFAULT NULL,
  `item` int(11) DEFAULT NULL,
  `descripcion` varchar(50) DEFAULT NULL,
  `valor` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='tabla para datos generales agrupadas por el codigo  e item. el valor puede o no existir';

-- Volcando datos para la tabla capital.configuracion: ~4 rows (aproximadamente)
DELETE FROM `configuracion`;
/*!40000 ALTER TABLE `configuracion` DISABLE KEYS */;
INSERT INTO `configuracion` (`codigo`, `item`, `descripcion`, `valor`) VALUES
	(0, 1, 'SUIZA', NULL),
	(0, 2, 'DEL JARDIN', NULL),
	(0, 3, 'KOKOMO', NULL),
	(0, 4, 'ASDASDASD', NULL);
/*!40000 ALTER TABLE `configuracion` ENABLE KEYS */;


-- Volcando estructura para tabla capital.departamento
CREATE TABLE IF NOT EXISTS `departamento` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `descripcion` varchar(500) COLLATE utf8_bin DEFAULT NULL,
  `fecha_alta` date DEFAULT NULL,
  `nivel_id` int(11) DEFAULT NULL,
  KEY `Índice 1` (`id`),
  KEY `FK_departamento_nivel` (`nivel_id`),
  CONSTRAINT `FK_departamento_nivel` FOREIGN KEY (`nivel_id`) REFERENCES `nivel` (`id_nivel`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- Volcando datos para la tabla capital.departamento: ~3 rows (aproximadamente)
DELETE FROM `departamento`;
/*!40000 ALTER TABLE `departamento` DISABLE KEYS */;
INSERT INTO `departamento` (`id`, `descripcion`, `fecha_alta`, `nivel_id`) VALUES
	(1, 'ADASDASDADASDA', '2018-07-12', 4),
	(2, 'Compras', '2019-01-30', 2),
	(3, 'Ventas', '2019-01-31', 1);
/*!40000 ALTER TABLE `departamento` ENABLE KEYS */;


-- Volcando estructura para tabla capital.empleado
CREATE TABLE IF NOT EXISTS `empleado` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `legajo` int(11) NOT NULL,
  `dni` int(11) NOT NULL,
  `nombre` varchar(200) COLLATE utf8_bin NOT NULL,
  `fecha_alta` date NOT NULL,
  `habilitado` tinyint(1) NOT NULL,
  KEY `Índice 1` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- Volcando datos para la tabla capital.empleado: ~2 rows (aproximadamente)
DELETE FROM `empleado`;
/*!40000 ALTER TABLE `empleado` DISABLE KEYS */;
INSERT INTO `empleado` (`id`, `legajo`, `dni`, `nombre`, `fecha_alta`, `habilitado`) VALUES
	(1, 123123, 12213323, 'asdasd', '2019-02-01', 0),
	(2, 23123, 12312322, 'otro mas', '2019-02-01', 0),
	(3, 1231321, 12323334, 'sdasda', '2019-02-01', 0);
/*!40000 ALTER TABLE `empleado` ENABLE KEYS */;


-- Volcando estructura para tabla capital.historico
CREATE TABLE IF NOT EXISTS `historico` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fecha` date NOT NULL,
  `empleado_id` int(11) NOT NULL,
  `puesto_id` int(11) NOT NULL,
  KEY `Índice 1` (`id`),
  KEY `FK_historico_empleado` (`empleado_id`),
  KEY `FK_historico_puesto` (`puesto_id`),
  CONSTRAINT `FK_historico_empleado` FOREIGN KEY (`empleado_id`) REFERENCES `empleado` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_historico_puesto` FOREIGN KEY (`puesto_id`) REFERENCES `puesto` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- Volcando datos para la tabla capital.historico: ~4 rows (aproximadamente)
DELETE FROM `historico`;
/*!40000 ALTER TABLE `historico` DISABLE KEYS */;
INSERT INTO `historico` (`id`, `fecha`, `empleado_id`, `puesto_id`) VALUES
	(1, '2018-01-01', 1, 3),
	(2, '2019-02-15', 1, 2),
	(3, '2019-02-15', 2, 3),
	(4, '2019-02-14', 3, 5);
/*!40000 ALTER TABLE `historico` ENABLE KEYS */;


-- Volcando estructura para tabla capital.menu
CREATE TABLE IF NOT EXISTS `menu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `titulo` varchar(50) DEFAULT NULL,
  `habilitado` tinyint(4) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla capital.menu: ~0 rows (aproximadamente)
DELETE FROM `menu`;
/*!40000 ALTER TABLE `menu` DISABLE KEYS */;
/*!40000 ALTER TABLE `menu` ENABLE KEYS */;


-- Volcando estructura para tabla capital.menu_elemento
CREATE TABLE IF NOT EXISTS `menu_elemento` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `menu_id` int(11) DEFAULT NULL,
  `titulo` varchar(50) DEFAULT NULL,
  `orden` tinyint(4) DEFAULT NULL,
  `padre_id` int(11) DEFAULT '0',
  `objeto_id_enlace` int(11) DEFAULT NULL,
  `tipo_objeto_enlace` int(11) DEFAULT NULL,
  `enlace_externo` varchar(150) DEFAULT NULL,
  `target_enlace` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla capital.menu_elemento: ~0 rows (aproximadamente)
DELETE FROM `menu_elemento`;
/*!40000 ALTER TABLE `menu_elemento` DISABLE KEYS */;
/*!40000 ALTER TABLE `menu_elemento` ENABLE KEYS */;


-- Volcando estructura para tabla capital.modulo
CREATE TABLE IF NOT EXISTS `modulo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `titulo` varchar(50) DEFAULT NULL,
  `accion` varchar(50) DEFAULT NULL,
  `es_menu` tinyint(4) DEFAULT '0',
  `hijos` tinyint(4) DEFAULT '0',
  `icono` varchar(20) DEFAULT NULL,
  `padre_id` int(11) DEFAULT NULL,
  `orden` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla capital.modulo: ~7 rows (aproximadamente)
DELETE FROM `modulo`;
/*!40000 ALTER TABLE `modulo` DISABLE KEYS */;
INSERT INTO `modulo` (`id`, `titulo`, `accion`, `es_menu`, `hijos`, `icono`, `padre_id`, `orden`) VALUES
	(1, 'Modulos', 'modulos/index', 1, 0, 'fa-server', 0, 1),
	(2, 'Perfiles', 'perfiles/index', 0, 0, 'fa-group', 0, 2),
	(3, 'Usuarios', 'usuarios/index', 0, 0, 'fa-user-plus', 0, 2),
	(5, 'Nivel', 'niveles/index', 0, 0, 'fa-sitemap', 0, 4),
	(6, 'Departamentos', 'departamentos/index', 0, 0, 'fa-building', 0, 2),
	(7, 'Puesto', 'puestos/index', 0, 0, 'fa-sitemap', 0, 4),
	(8, 'Empleados', 'empleados/index', 0, 0, 'fa-user', 0, 1);
/*!40000 ALTER TABLE `modulo` ENABLE KEYS */;


-- Volcando estructura para tabla capital.nivel
CREATE TABLE IF NOT EXISTS `nivel` (
  `id_nivel` int(11) NOT NULL AUTO_INCREMENT,
  `descripcion` varchar(50) COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`id_nivel`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- Volcando datos para la tabla capital.nivel: ~4 rows (aproximadamente)
DELETE FROM `nivel`;
/*!40000 ALTER TABLE `nivel` DISABLE KEYS */;
INSERT INTO `nivel` (`id_nivel`, `descripcion`) VALUES
	(1, 'prueb'),
	(2, 'nivel'),
	(4, 'nivel3'),
	(5, 'prueba2'),
	(8, 'asdasd');
/*!40000 ALTER TABLE `nivel` ENABLE KEYS */;


-- Volcando estructura para tabla capital.perfil
CREATE TABLE IF NOT EXISTS `perfil` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(150) DEFAULT NULL,
  `codigo` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla capital.perfil: ~0 rows (aproximadamente)
DELETE FROM `perfil`;
/*!40000 ALTER TABLE `perfil` DISABLE KEYS */;
INSERT INTO `perfil` (`id`, `nombre`, `codigo`) VALUES
	(1, 'Administrador', 1);
/*!40000 ALTER TABLE `perfil` ENABLE KEYS */;


-- Volcando estructura para tabla capital.permiso
CREATE TABLE IF NOT EXISTS `permiso` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `perfil_id` int(11) DEFAULT NULL,
  `modulo_id` int(11) DEFAULT NULL,
  `accion` varchar(150) DEFAULT NULL,
  `alta` tinyint(4) DEFAULT '0',
  `baja` tinyint(4) DEFAULT '0',
  `mod` tinyint(4) DEFAULT '0',
  `listado` tinyint(4) DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla capital.permiso: ~9 rows (aproximadamente)
DELETE FROM `permiso`;
/*!40000 ALTER TABLE `permiso` DISABLE KEYS */;
INSERT INTO `permiso` (`id`, `perfil_id`, `modulo_id`, `accion`, `alta`, `baja`, `mod`, `listado`) VALUES
	(1, 1, 1, 'modulos', 1, 1, 1, 1),
	(2, 1, 2, 'perfiles', 1, 1, 1, 1),
	(3, 1, 3, 'usuarios', 1, 1, 1, 1),
	(8, 1, 8, 'configuraciones', 1, 1, 1, 1),
	(9, 1, 9, 'proveedores', 1, 1, 1, 1),
	(10, 1, 10, 'equipos', 1, 1, 1, 1),
	(11, 1, 11, 'perifericos', 1, 1, 1, 1),
	(13, 1, 5, 'niveles', 1, 1, 1, 1),
	(14, 1, 6, 'departamentos', 1, 1, 1, 1),
	(15, 1, 7, 'puestos', 1, 1, 1, 1),
	(16, 1, 8, 'empleados', 1, 1, 1, 1);
/*!40000 ALTER TABLE `permiso` ENABLE KEYS */;


-- Volcando estructura para tabla capital.puesto
CREATE TABLE IF NOT EXISTS `puesto` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(150) COLLATE utf8_bin DEFAULT NULL,
  `descripcion` mediumtext COLLATE utf8_bin,
  `fecha_alta` date DEFAULT NULL,
  `id_departamento` int(11) DEFAULT NULL,
  KEY `Índice 1` (`id`),
  KEY `FK_puesto_departamento` (`id_departamento`),
  CONSTRAINT `FK_puesto_departamento` FOREIGN KEY (`id_departamento`) REFERENCES `departamento` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- Volcando datos para la tabla capital.puesto: ~4 rows (aproximadamente)
DELETE FROM `puesto`;
/*!40000 ALTER TABLE `puesto` DISABLE KEYS */;
INSERT INTO `puesto` (`id`, `nombre`, `descripcion`, `fecha_alta`, `id_departamento`) VALUES
	(2, 'Encargado', 'encargado de las ventas de salon   ', '2019-01-30', 3),
	(3, 'vendedor de salon', '   vendedor de salon', '2019-01-30', 3),
	(4, 'vendedor de salon', '      vendedor de salon      ', '2019-01-30', 3),
	(5, 'Encargado', 'asdasdasd   ', '2019-02-01', 2);
/*!40000 ALTER TABLE `puesto` ENABLE KEYS */;


-- Volcando estructura para tabla capital.usuario
CREATE TABLE IF NOT EXISTS `usuario` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(150) DEFAULT NULL,
  `apellido` varchar(150) DEFAULT NULL,
  `email` varchar(150) DEFAULT NULL,
  `habilitado` tinyint(4) DEFAULT '1',
  `perfil_id` int(11) DEFAULT NULL,
  `fechaalta` datetime DEFAULT NULL,
  `ultimo_acceso` datetime DEFAULT NULL,
  `password` varchar(150) DEFAULT NULL,
  `usuarioaud` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `perfil_id` (`perfil_id`),
  KEY `usuarioaud` (`usuarioaud`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla capital.usuario: ~0 rows (aproximadamente)
DELETE FROM `usuario`;
/*!40000 ALTER TABLE `usuario` DISABLE KEYS */;
INSERT INTO `usuario` (`id`, `nombre`, `apellido`, `email`, `habilitado`, `perfil_id`, `fechaalta`, `ultimo_acceso`, `password`, `usuarioaud`) VALUES
	(1, 'admin', 'Administrador', 'admin@admin.com', 1, 1, '2015-11-10 17:00:26', '2019-02-01 09:07:54', '820653fa58804f71514e418a6b269a07', 1);
/*!40000 ALTER TABLE `usuario` ENABLE KEYS */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
